from setuptools import setup

setup(
    name="hbautil",
    version="1.0",
    packages=["hbautil"],
    include_package_data=True,
    install_requires=[
        # See note in "pecas-routines"
        "psycopg2-binary",
        "pyyaml>=5.3.1",
        "setuptools-git",
    ]
)
