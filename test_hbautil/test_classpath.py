import os
import unittest
from os.path import join

from hbautil.classpath import jar_any_version, build_classpath_in_path

jar_path = join("test", "classpath", "AllYears", "Code")


class TestClasspath(unittest.TestCase):
    def test_jar_any_version(self):
        self.assertEqual(join(jar_path, "foo123.jar"), jar_any_version(jar_path, "foo"))
        self.assertEqual(join(jar_path, "bar2345.jar"), jar_any_version(jar_path, "bar"))
        with self.assertRaises(FileNotFoundError):
            jar_any_version(jar_path, "missing")

    def test_build_classpath_other_jars(self):
        self.assertEqual(
            os.pathsep.join([
                jar_path,
                join(jar_path, "spam369.jar"),
                join(jar_path, "eggs248.jar"),
                join(jar_path, "foo123.jar"),
                join(jar_path, "bar2345.jar"),
                join(jar_path, "fooey-2.6.2.jar"),
                join(jar_path, "numbers4us3-1.4.jar"),
                join(jar_path, "timeless.jar"),
                join(jar_path, "petit4.jar")
            ]),
            build_classpath_in_path(
                jar_path,
                specific_jars=[
                    "spam369.jar",
                    "eggs248.jar",
                ],
                other_jars=[
                    "foo",
                    "bar",
                    "fooey",
                    "numbers4us",
                    "timeless",
                    "petit",
                ]
            )
        )
