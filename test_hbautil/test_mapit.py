import unittest
from os.path import join

from hbautil import mapit
import hbautil.mapit_files as mf
from pecas_test import mockmapit, mocksettings

ps = mocksettings.PecasSettings().with_machine_settings().with_test_sd().clone_with(
    parcels_table="parcels",
    scendir=join("test", "mapit"),
)


class TestMapit(unittest.TestCase):
    """
    Tests the mapit upload sequences.

    The 2016 directory has been intentionally left empty to make sure that the uploader
    doesn't crash if there are missing files.
    """

    @classmethod
    def setUpClass(cls):
        cls.standard_ps = ps.with_mapit("T01", schema="output", database="hba_mapit_test").between_years(2011, 2016)
        mockmapit.create_database(cls.standard_ps)

    def setUp(self):
        mapit.mapit_querier(self.standard_ps).query("select output.clean_up_tables_for_scenario('T01')")

    def testUploadEagerAA(self):
        mapit.load_outputs_for_year(self.standard_ps, 2011, mf.EAGER_AA)
        mapit.load_outputs_for_year(self.standard_ps, 2016, mf.EAGER_AA)
        self.assertCount(2, "output.all_activity_summary")
        self.assertCount(2, "output.all_exchange_results_totals")

    def testUploadLazyAA(self):
        mapit.load_outputs_for_year(self.standard_ps, 2011, mf.LAZY_AA)
        mapit.load_outputs_for_year(self.standard_ps, 2016, mf.LAZY_AA)
        self.assertCount(4, "output.all_activity_locations_2")

    def testUploadEagerSD(self):
        mapit.load_outputs_for_year(self.standard_ps, 2011, mf.EAGER_SD)
        mapit.load_outputs_for_year(self.standard_ps, 2016, mf.EAGER_SD)
        self.assertCount(4, "output.all_floorspacesd")

    def testUploadEmployment(self):
        mapit.load_outputs_for_year(self.standard_ps, 2011, mf.LAZY_EMP)
        mapit.load_outputs_for_year(self.standard_ps, 2016, mf.LAZY_EMP)
        self.assertCount(8, "output.all_employment")

    def testClearSpecific(self):
        mapit.load_outputs_for_year(self.standard_ps, 2011, mf.LAZY_AA)
        mapit.load_outputs_for_year(self.standard_ps, 2012, mf.LAZY_AA)
        self.assertCount(8, "output.all_activity_locations_2")
        mapit.clear_specific_uploads(self.standard_ps, [2011], [mf.LAZY_AA])
        self.assertCount(4, "output.all_activity_locations_2")

    def assertCount(self, expected_rows, table):
        with mapit.mapit_querier(self.standard_ps) as q:
            self.assertEqual(expected_rows, q.query_scalar("select count(*) from {table}", table=table))
