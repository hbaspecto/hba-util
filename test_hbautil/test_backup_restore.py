import shutil
from os.path import join

from hbautil import sd_backup_restore
from pecas_test import mockscen, mocksettings, filetest

base_dir = join("test", "backup_restore")
gen_ps = mocksettings.PecasSettings("I269br").with_machine_settings().with_test_sd(
    "i269br"
)


class TestBackupRestore(filetest.FileTestCase):
    @classmethod
    def setUpClass(cls):
        mockscen.create_database(gen_ps, geometry=True)

    def testBackupRestoreNoData(self):
        scendir = join(base_dir, "no_data")
        self.do_backup_restore_test(scendir)

    def testBackupRestoreSequence(self):
        scendir = join(base_dir, "sequence")
        self.do_backup_restore_test(scendir)

    def do_backup_restore_test(self, scendir):
        ps = gen_ps.with_scenario_directory(scendir)

        backup_dir = join(scendir, "sd_backup")
        expected_dir = join(scendir, "sd_backup_cmp")
        shutil.rmtree(backup_dir, ignore_errors=True)
        shutil.copytree(expected_dir, backup_dir)
        sd_backup_restore.restore_sd(
            ps,
            combine=True, overwrite=True, use_environ=False, schema=ps.sd_schema, threaded=False
        )
        sd_backup_restore.backup_sd(
            ps,
            split=True, overwrite=True, use_environ=False, schema=ps.sd_schema, threaded=False
        )
        self.assertDirectoriesEqual(expected_dir, backup_dir, ignore_lines_starting_with="--")
