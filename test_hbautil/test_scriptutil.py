import os
import unittest
from os.path import join

from pecas_test.testutil import recreate_test_folder
from hbautil import scriptutil as su


class TestFileHandling(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.scendir = join("test", "file_handling")
        recreate_test_folder(cls.scendir)

    def testReplaceWithBackupUnobstructed(self):
        scendir = join(self.scendir, "unobstructed")
        recreate_test_folder(scendir)
        self._write_file(join(scendir, "foo.txt"), "Testing!")
        su.replace_with_backup(join(scendir, "foo.txt"), join(scendir, "bar.txt"))
        self._assert_file("Testing!", join(scendir, "bar.txt"))
        self._assert_file_count(1, scendir)

    def testReplaceWithBackupObstructed(self):
        scendir = join(self.scendir, "obstructed")
        recreate_test_folder(scendir)
        self._write_file(join(scendir, "foo.txt"), "Testing!")
        self._write_file(join(scendir, "bar.txt"), "Obstruction")
        su.replace_with_backup(join(scendir, "foo.txt"), join(scendir, "bar.txt"))
        self._assert_file("Testing!", join(scendir, "bar.txt"))
        self._assert_file("Obstruction", join(scendir, "bar_backup.txt"))
        self._assert_file_count(2, scendir)

    @staticmethod
    def _write_file(path, text):
        with open(path, "w") as f:
            f.write(text)

    def _assert_file(self, expected_text, path):
        with open(path, "r") as f:
            self.assertEqual(expected_text, f.readline())

    def _assert_file_count(self, expected_count, directory):
        self.assertEqual(expected_count, len(os.listdir(directory)))
