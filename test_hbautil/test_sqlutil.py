import unittest

from psycopg2 import ProgrammingError, OperationalError, extensions

from hbautil.sqlutil import Querier


class Marker(object):
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return self.name


START_TRANSACTION = Marker("START_TRANSACTION")
MAKE_CURSOR = Marker("MAKE_CURSOR")
START_CURSOR = Marker("START_CURSOR")
CLOSE_CURSOR = Marker("CLOSE_CURSOR")
COMMIT = Marker("COMMIT")
ROLLBACK = Marker("ROLLBACK")
CLOSE_CONNECTION = Marker("CLOSE_CONNECTION")


def transaction(*logs):
    return ([START_TRANSACTION, MAKE_CURSOR, START_CURSOR] +
            list(logs) +
            [CLOSE_CURSOR, COMMIT])


def aborted_transaction(*logs):
    return ([START_TRANSACTION, MAKE_CURSOR, START_CURSOR] +
            list(logs) +
            [CLOSE_CURSOR, ROLLBACK])


def connection(logs):
    return list(logs) + [CLOSE_CONNECTION]


class TestQuerier(unittest.TestCase):
    def setUp(self):
        self.querier = Querier(self.connect)
        self.const_querier = Querier(self.connect, spam=216, eggs="scrambled")
        self.debug_log = DebugLog()
        self.debug_querier = Querier(self.connect, self.debug_log)

    def connect(self):
        self.connector = Connector()
        return self.connector

    def bad_connect(self):
        raise OperationalError("Does not compute")

    def test_simple_query(self):
        with self.querier as querier:
            querier.query("select * from foo")

        self.assertLogIs(
            connection(transaction("select * from foo")))

    def test_multiple_query(self):
        with self.querier as querier:
            querier.query("select * from foo")
            querier.query("select * from bar")
            querier.query("select * from bazzywaz")

        self.assertLogIs(
            connection(
                transaction("select * from foo") +
                transaction("select * from bar") +
                transaction("select * from bazzywaz")))

    def test_transaction(self):
        with self.querier.transaction() as tr:
            tr.query("select * from foo")
            tr.query("select * from bar")
            tr.query("select * from bazzywaz")

        self.assertLogIs(
            connection(
                transaction("select * from foo",
                            "select * from bar",
                            "select * from bazzywaz")))

    def test_hybrid_transaction(self):
        with self.querier as querier:
            querier.query("select * from foo")
            with querier.transaction() as tr:
                tr.query("select * from bar")
                tr.query("select * from bazzywaz")

        self.assertLogIs(
            connection(
                transaction("select * from foo") +
                transaction("select * from bar",
                            "select * from bazzywaz")))

    def test_multiple_with(self):
        with self.querier as querier:
            querier.query("select * from foo")

        connector1 = self.connector
        self.assertLogIs(
            connection(
                transaction("select * from foo")))

        with self.querier as querier:
            querier.query("select * from bar")

        self.assertLogIs(
            connection(
                transaction("select * from foo")), connector=connector1)
        self.assertLogIs(
            connection(
                transaction("select * from bar")))

    def test_multiple_transaction_with(self):
        with self.querier.transaction() as tr:
            tr.query("select * from foo")

        connector1 = self.connector
        self.assertLogIs(
            connection(
                transaction("select * from foo")))

        with self.querier.transaction() as tr:
            tr.query("select * from bar")

        self.assertLogIs(
            connection(
                transaction("select * from foo")), connector=connector1)
        self.assertLogIs(
            connection(
                transaction("select * from bar")))

    def test_subs(self):
        with self.querier as querier:
            querier.query(
                "select * from foo_{num} "
                "where quux = %(num)s and queex = %(x)s",
                num=42, x=3.1416)

        self.assertLogIs(
            connection(transaction(
                "select * from foo_42 where quux = 42 and queex = 3.1416")))

    def test_subs_transaction(self):
        with self.querier.transaction() as querier:
            querier.query(
                "select * from foo_{num} "
                "where quux = %(x)s and queex_{x} = 3.1416",
                num=42, x="pi")

        self.assertLogIs(
            connection(transaction(
                "select * from foo_42 where quux = 'pi' and queex_pi = 3.1416"
            )))

    def test_global_subs(self):
        with self.const_querier as querier:
            querier.query(
                "select * from foo_{num} "
                "where quux_{spam} = %(num)s and queex_{eggs} = %(spam)s",
                num=42)
            querier.query(
                "select * from {eggs}", eggs="fried")

        self.assertLogIs(
            connection(
                transaction(
                    "select * from foo_42 "
                    "where quux_216 = 42 and queex_scrambled = 216") +
                transaction("select * from fried")))

    def test_debug(self):
        debug_log = DebugLog()
        with self.querier as querier:
            querier.query("select * from foo", debug_log)
            querier.query("select * from bar")
            querier.query("select * from baz{suf}", debug_log, suf="zywaz")

        self.assertLogIs(
            connection(
                transaction("select * from foo") +
                transaction("select * from bar") +
                transaction("select * from bazzywaz")))

        self.assertEqual(
            ["Executing query:", "select * from foo", "-" * 80,
             "Executing query:", "select * from bazzywaz", "-" * 80],
            debug_log.debug_log)

    def test_global_debug(self):
        with self.debug_querier as querier:
            querier.query("select * from foo")
            querier.query("select * from bar", debug_log=None)
            querier.query("select * from baz{suf}", suf="zywaz")

        self.assertLogIs(
            connection(
                transaction("select * from foo") +
                transaction("select * from bar") +
                transaction("select * from bazzywaz")))

        self.assertEqual(
            ["Executing query:", "select * from foo", "-" * 80,
             "Executing query:", "select * from bazzywaz", "-" * 80],
            self.debug_log.debug_log)

    def test_query_with_result(self):
        with self.querier as querier:
            foo_result = querier.query("select azerbaijan from foo")
            bar_result = querier.query("select * from bar")
            foo_42_result = querier.query("select * from foo_{num}", num=42)

        self.assertEqual(42, foo_result)
        self.assertIsNone(bar_result)
        self.assertEqual(42, foo_42_result)

    def test_bad_query(self):
        with self.assertRaises(ProgrammingError):
            with self.querier as querier:
                querier.query("select error from foo")

        self.assertLogIs(
            connection(
                aborted_transaction("Bad query: select error from foo")))

    def test_bad_query_transaction(self):
        with self.assertRaises(ProgrammingError):
            with self.querier.transaction() as tr:
                tr.query("select * from foo")
                tr.query("select error from bar")
                tr.query("select * from bazzywaz")

        self.assertLogIs(
            connection(
                aborted_transaction("select * from foo",
                                    "Bad query: select error from bar")))

    def test_bad_connection(self):
        with self.assertRaises(OperationalError):
            with Querier(self.bad_connect) as querier:
                querier.query("select * from foo")

    def test_bad_connection_transaction(self):
        with self.assertRaises(OperationalError):
            with Querier(self.bad_connect).transaction() as tr:
                tr.query("select * from foo")

    def assertLogIs(self, expected, connector=None):
        if connector is None:
            connector = self.connector
        self.assertEqual(expected, connector.log)


class Connector(object):
    def __init__(self):
        self.log = []

    def __enter__(self):
        self.log.append(START_TRANSACTION)
        return self

    def __exit__(self, exec_type, *args):
        if exec_type is None:
            self.log.append(COMMIT)
        else:
            self.log.append(ROLLBACK)

    def cursor(self, **kwargs):
        self.log.append(MAKE_CURSOR)
        return Cursor(self)

    def close(self):
        self.log.append(CLOSE_CONNECTION)


class Cursor(object):
    def __init__(self, conn):
        self.conn = conn
        self.result = None

    def __enter__(self):
        self.conn.log.append(START_CURSOR)
        return self

    def __exit__(self, *args):
        self.conn.log.append(CLOSE_CURSOR)

    def mogrify(self, query, vars):
        vars = {key: extensions.adapt(val).getquoted().decode()
                for key, val in vars.items()}
        return bytes(query % vars, "utf8")

    def execute(self, query, vars=None):
        if vars is None:
            vars = {}
        query = self.mogrify(query, vars).decode()
        if "error" in query:
            msg = "Bad query: " + query
            self.conn.log.append("Bad query: " + query)
            raise ProgrammingError(msg)
        else:
            self.conn.log.append(query)
            if "foo" in query:
                self.result = 42
            else:
                self.result = None

    def fetchall(self):
        if self.result is None:
            raise ProgrammingError
        else:
            return self.result


class DebugLog(object):
    def __init__(self):
        self.debug_log = []

    def log(self, line):
        self.debug_log.append(line)
