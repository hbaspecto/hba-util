import os
import unittest

from hbautil import settings


class TestSettings(unittest.TestCase):
    def testFromPython(self):
        ex = settings.from_python("test/example_settings.py")

        self.assertEqual(12, ex.foo)
        self.assertEqual("Something", ex.bar)
        self.assertEqual(["This", "That", "The Other"], ex.baz)
        self.assertEqual([12, 14, 15, 16, 18, 20], sorted(ex.spam))
        self.assertEqual(os.path.join("foo", "bar"), ex.ham)

        self.assertEqual(["bar", "baz", "dictionary", "foo", "ham", "scendir", "spam"], sorted(ex.as_dict.keys()))

    def testFromYaml(self):
        ex = settings.from_yaml("test/example.yml", pattern_lists=["spam", "dummy"])

        self.assertEqual(12, ex.foo)
        self.assertEqual("Something", ex.bar)
        self.assertEqual(["This", "That", "The Other"], ex.baz)
        self.assertEqual([12, 14, 15, 16, 18, 20], sorted(ex.spam))

    def testFromYamlEmptyFile(self):
        self.assertEqual({}, settings.from_yaml("test/empty.yml").as_dict)

    def testPatternLists(self):
        ex = settings.from_yaml("test/pattern_list_example.yml", pattern_lists=["foo", "bar", "baz", "quux"])

        self.assertEqual(list(range(25, 43)), ex.foo)
        self.assertEqual([17, 80, 212], ex.bar)
        self.assertEqual(list(range(12, 30, 4)), ex.baz)
        self.assertEqual([5, 6, 7, 8, 9, 13, 16], ex.quux)

    def testYamlRoundTrip(self):
        yaml = settings.template_from_yaml("test/example_template.yml")
        settings.template_to_yaml("test/example_template_round_trip.yml", yaml)

        with open("test/example_template.yml") as exp, open("test/example_template_round_trip.yml") as act:
            self.assertEqual(list(exp), list(act))

    def testYamlTemplateUpdate(self):
        template = settings.template_from_yaml("test/example_template.yml", pattern_lists=["spam", "dummy"])
        new_file = template.update(settings.from_python("test/example_settings.py"))
        settings.template_to_yaml("test/example_template_out_actual.yml", new_file)

        with open("test/example_template_out_expected.yml") as exp, open("test/example_template_out_actual.yml") as act:
            self.assertEqual(list(exp), list(act))

    def testTemplateUpdatePatternLists(self):
        pattern_lists = [
            "l_l", "l_ss", "l_sse", "l_ssi", "l_ssie", "l_e",
            "ss_l", "ss_ss", "ss_sse", "ss_ssi", "ss_ssie", "ss_e",
            "sse_l", "sse_ss", "sse_sse", "sse_ssi", "sse_ssie", "sse_e",
            "ssi_l", "ssi_ss", "ssi_sse", "ssi_ssi", "ssi_ssie", "ssi_e",
            "ssie_l", "ssie_ss", "ssie_sse", "ssie_ssi", "ssie_ssie", "ssie_e",
        ]

        template = settings.template_from_yaml("test/pattern_list_example_template.yml", pattern_lists=pattern_lists)
        new_file = template.update(
            settings.from_yaml("test/pattern_list_example_update.yml", pattern_lists=pattern_lists)
        )
        settings.template_to_yaml("test/pattern_list_example_actual.yml", new_file)

        with open("test/pattern_list_example_expected.yml") as exp, open("test/pattern_list_example_actual.yml") as act:
            self.assertEqual(list(exp), list(act))
