import glob
import os
import re
from os.path import join, basename, splitext
from typing import List, Mapping, Any


def build_classpath_in_path(path: str, specific_jars: List[str], other_jars: List[str]):
    return build_classpath(
        path,
        *[join(path, jar) for jar in specific_jars],
        *[jar_any_version(path, jar) for jar in other_jars],
    )


def build_classpath(*args: str):
    return os.pathsep.join(args)


def jar_any_version(path: str, name: str):
    #print("Looking for "+str(name)+" in "+str(path))
    result = glob.glob(join(path, name + "*.jar"))
    if not result:
        raise FileNotFoundError(f"No jar file with name {name}")
    #print("found "+str(result))
    result1 = _best_matches_only(result, name)
    if result1:
        result = result1

    def extract_numbers(fname: str):
        return tuple(map(int, re.findall("\\d+", fname)))

    result.sort(key=extract_numbers)
    return result[-1]


def _best_matches_only(paths: List[str], target_name: str) -> List[str]:
    base_names = [basename(path) for path in paths]

    def number_start(fname: str):
        numbers = re.search("-?\\d+[\\d\\-.]+", fname)
        if numbers is None:
            return len(splitext(fname)[0])
        return numbers.start()

    name_parts = [
        fname[:number_start(fname)]
        for fname in base_names
    ]
    return [path for i, path in enumerate(paths) if name_parts[i] == target_name]


def vm_properties(properties: Mapping[str, Any]) -> List[str]:
    """
    Returns a list of command line arguments to set the specified system properties on a Java VM subprocess call
    """
    return ["-D{}={}".format(name, value) for name, value in properties.items()]
