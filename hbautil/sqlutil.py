from os.path import dirname
import re

from psycopg2 import ProgrammingError
from psycopg2.extras import execute_batch, NamedTupleCursor
import hbautil.pecassetup


_INHERIT = object()

BUFSIZE = 2**10


class Querier:
    def __init__(self, connect, debug_log=None, named_tuples=False, cursor_factory=None, **kwargs):
        self.connect = connect
        self.debug_log = debug_log
        self.named_tuples = named_tuples
        if named_tuples:
            self.cursor_factory = NamedTupleCursor
        else:
            self.cursor_factory = cursor_factory
        self.kwargs = kwargs
        self._entered = False
    
    def __enter__(self):
        self._conn = self.connect()
        self._entered = True
        return self

    # noinspection PyBroadException
    def __exit__(self, *args):
        try:
            self._entered = False
            self._conn.close()
        except Exception:
            pass
    
    def query(self, query, debug_log=_INHERIT, literal=False, named_tuples=_INHERIT, cursor_factory=_INHERIT, **kwargs):
        with self.transaction(named_tuples=named_tuples, cursor_factory=cursor_factory) as tr:
            return tr.query(query, debug_log, literal, **kwargs)

    def query_scalar(self, query, debug_log=_INHERIT, literal=False, **kwargs):
        """
        Performs the specified query, returning the first field of the first row of the query result.
        Use this if the query computes a single value, such as the number of rows in a table.
        """
        return self.query(query, debug_log=debug_log, literal=literal, **kwargs)[0][0]
    
    def query_many(self, query, argslist, debug_log=_INHERIT, **kwargs):
        with self.transaction() as tr:
            tr.query_many(query, argslist, debug_log, **kwargs)

    def query_external(self, fname, start=None, end=None, debug_log=_INHERIT, literal=False, **kwargs):
        with self.transaction() as tr:
            tr.query_external(fname, start, end, debug_log, literal, **kwargs)

    def callproc(self, procname, parameters=_INHERIT, debug_log=_INHERIT, **kwargs):
        with self.transaction() as tr:
            tr.callproc(procname, parameters, debug_log, **kwargs)

    def dump_to_csv(self, query, fname, debug_log=_INHERIT, **kwargs):
        with self.transaction() as tr:
            tr.dump_to_csv(query, fname, debug_log, **kwargs)

    def load_from_csv(self, table, fname, header=True, debug_log=_INHERIT, **kwargs):
        with self.transaction() as tr:
            tr.load_from_csv(table, fname, header, debug_log, **kwargs)
    
    def transaction(self, named_tuples=_INHERIT, cursor_factory=_INHERIT, **kwargs):
        return Transaction(self, named_tuples=named_tuples, cursor_factory=cursor_factory, **kwargs)

    def vacuum(self, table, analyze=False, **kwargs):
        with self.connect() as conn:
            conn.autocommit = True
            cur = conn.cursor(cursor_factory=self.cursor_factory)
            full_kwargs = dict(self.kwargs)
            full_kwargs.update(kwargs)
            query = "vacuum analyze {}" if analyze else "vacuum {}"
            query = query.format(table).format(**full_kwargs)
            cur.execute(query)


class Transaction:
    def __init__(self, querier, named_tuples, cursor_factory, **kwargs):
        self._querier = querier
        if named_tuples is _INHERIT:
            named_tuples = querier.named_tuples
        if cursor_factory is _INHERIT:
            cursor_factory = querier.cursor_factory
        self._named_tuples = named_tuples
        if named_tuples:
            self._cursor_factory = NamedTupleCursor
        else:
            self._cursor_factory = cursor_factory
        self._kwargs = kwargs
        self._collapsed_entry = False
    
    def __enter__(self):
        if not self._querier._entered:
            self._querier.__enter__()
            self._collapsed_entry = True
        self._conn = self._querier._conn
        self._conn.__enter__()
        self._cur = self._conn.cursor(cursor_factory=self._cursor_factory)
        self._cur.__enter__()
        return self

    # noinspection PyBroadException
    def __exit__(self, *args):
        try:
            try:
                self._cur.__exit__(*args)
            finally:
                try:
                    self._conn.__exit__(*args)
                finally:
                    if self._collapsed_entry:
                        self._querier.__exit__(*args)
        except Exception:
            pass
    
    def query(self, query, debug_log=_INHERIT, literal=False, **kwargs):
        debug_log = self._real_debug_log(debug_log)

        if not literal:
            kwargs = self._full_kwargs(kwargs)
            query = query.format(**kwargs)

        print("Executing query :")
        debug_log.log("Executing query:")
        if literal:
            print(query)
            debug_log.log(query)
        else:
            print (self._cur.mogrify(query, kwargs).decode())
            debug_log.log(self._cur.mogrify(query, kwargs).decode())
        self._draw_line(debug_log)

        if literal:
            self._cur.execute(query)
        else:
            try:
                self._cur.execute(query, kwargs)
            except Exception as error:
                sqlstring = re.sub('%\\(([a-zA-Z0-9_]+)\\)s','{\\1}',query)
                print ("Error:", error, "Running query: ",sqlstring.format(**kwargs))
                raise
        try:
            result = self._cur.fetchall()
            return result
        except ProgrammingError:
            return None

    def query_scalar(self, query, debug_log=_INHERIT, literal=False, **kwargs):
        """
        Performs the specified query, returning the first field of the first row of the query result.
        Use this if the query computes a single value, such as the number of rows in a table.
        """
        return self.query(query, debug_log=debug_log, literal=literal, **kwargs)[0][0]
    
    def query_many(self, query, argslist, debug_log=_INHERIT, **kwargs):
        kwargs = self._full_kwargs(kwargs)
        debug_log = self._real_debug_log(debug_log)
        query = query.format(**kwargs)
        
        debug_log.log("Executing batch query:")
        debug_log.log(query)
        self._draw_line(debug_log)
        
        execute_batch(self._cur, query, argslist)
    
    def query_external(self, fname, start=None, end=None, debug_log=_INHERIT, literal=False, **kwargs):
        result = None
        
        with open(fname, encoding="utf-8-sig") as fin:
            lines = list(fin)
            it = iter(lines)
            if start is not None:
                for line in it:
                    line = line.strip()
                    if line.startswith("--"):
                        line = line[2:].strip()
                        if line == start:
                            break

            finished = False
            while not finished:
                query = ""
                for line in it:
                    if line.startswith("--"):
                        if end is not None:
                            comment = line[2:].strip()
                            if comment == end:
                                finished = True
                                break
                    else:
                        if line.strip():
                            query += line
                else:
                    finished = True
                
                if not query.strip():
                    continue
                
                new_result = self.query(query, debug_log, literal, **kwargs)
                if new_result is not None:
                    result = new_result
                    
        return result

    def callproc(self, procname, parameters=_INHERIT, debug_log=_INHERIT, **kwargs):
        kwargs = self._full_kwargs(kwargs)
        debug_log = self._real_debug_log(debug_log)
        if parameters is _INHERIT:
            parameters = []
        procname = procname.format(**kwargs)

        debug_log.log("Calling procedure {} with parameters {}".format(procname, parameters))
        self._draw_line(debug_log)

        self._cur.callproc(procname, parameters)

    def dump_to_csv(self, query, fname, debug_log=_INHERIT, **kwargs):
        kwargs = self._full_kwargs(kwargs)
        debug_log = self._real_debug_log(debug_log)
        query = self._cur.mogrify(query.format(**kwargs), kwargs).decode()
        fname = fname.format(**kwargs)

        debug_log.log("Dumping to file {} results of query:".format(fname))
        debug_log.log(query)
        self._draw_line(debug_log)

        with open(fname, "w", buffering=BUFSIZE) as outf:
            self._cur.copy_expert(
                    "copy ({}) to stdout csv header".format(query), outf)

    def smart_load_from_csv(self, table, csv_file, debug_log=_INHERIT):
        from hbautil.mapit import map_table_names_to_csv_headers
        debug_log = self._real_debug_log(debug_log)
        try:
            table_col_list = map_table_names_to_csv_headers(table, csv_file, tr=self)
            coldefs = ",\n".join(
                f"{col[1]} {col[2]}"
                + ("" if col[3] is None else f"({col[3]})")
                for col in table_col_list
            )

            temp_table_name="bob_is_temporary"
            print ("Temp table name is "+str(temp_table_name))
            self.query('create temporary table {} (\n{}\n);'.format(temp_table_name, coldefs),
                       literal=True)

            self.load_from_csv(temp_table_name, csv_file, use_tempfile=True)

            counter = self.query('SELECT count(*) FROM {tmp};', tmp=temp_table_name)[0][0]

            matched_cols = []
            for colitem in table_col_list:
                if colitem[1].startswith("_unmatched_"):
                    continue
                matched_cols.append(colitem)

            cols = ", ".join([col[1] for col in matched_cols])
            self.query(
                "INSERT INTO {table} ({cols}) "
                "SELECT {cols} from {tmp}",
                cols=cols, tmp=temp_table_name, table=table
            )

            debug_log.log("%d record(s) added to %s" % (counter, table))
        except IOError:  # if file does not exist. ex. ActivityLocations2_6k.csv not created every year.
            debug_log.log("NOTICE: No such file or directory:" + csv_file)

    def load_from_csv(self, table, fname, header=True, debug_log=_INHERIT, use_tempfile=False, **kwargs):
        import tempfile
        import shutil
        import os
        kwargs = self._full_kwargs(kwargs)
        debug_log = self._real_debug_log(debug_log)
        table = table.format(**kwargs)
        fname = fname.format(**kwargs)

        debug_log.log("Loading file {} into table {}".format(fname, table))
        self._draw_line(debug_log)
        if use_tempfile:
            fd, temp_path = tempfile.mkstemp(dir=dirname(fname))
            shutil.copy2(fname, temp_path)
            real_fname = temp_path
        else:
            real_fname = fname
        try:
            with open(real_fname, "r", buffering=BUFSIZE) as outf:
                self._cur.copy_expert(
                        "copy {} from stdin csv{}".format(table, " header" if header else ""), outf)
        finally:
            if use_tempfile:
                # noinspection PyUnboundLocalVariable
                os.close(fd)
                os.remove(real_fname)

    def _full_kwargs(self, kwargs):
        full_kwargs = dict(self._querier.kwargs)
        full_kwargs.update(self._kwargs)
        full_kwargs.update(kwargs)
        return full_kwargs

    def _real_debug_log(self, debug_log):
        if debug_log is _INHERIT:
            debug_log = self._querier.debug_log
        if debug_log is None:
            debug_log = NullLogger()
        return debug_log

    @staticmethod
    def _draw_line(debug_log):
        return debug_log.log("-" * 80)

        
class NullLogger:
    def log(self, text):
        pass
