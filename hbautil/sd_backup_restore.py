# -*- coding: utf-8 -*-

"""
Routines for backing up and restoring SD database.

Can be called from command line, or imported as a module
  Exports:
    • backup_sd()
    • restore_sd()
    • svn_commit_sd()
    • SDBackupError
    • SDRestoreError
    • SDCommitError
"""

from argparse import ArgumentParser
from collections import namedtuple
from glob import glob
from os import access, environ, listdir, mkdir, path, remove, W_OK, rename
from queue import Queue
from subprocess import Popen, PIPE, check_output, CalledProcessError
from sys import platform, stdout, stderr
from threading import Thread

import logging

import psycopg2
from psycopg2 import connect, ProgrammingError as SQLError
from psycopg2.extras import DictCursor
from psycopg2.sql import Identifier, SQL

import pecas_routines as pr
from hbautil import svnutil
from hbautil.pg_dump_splitsort import Splitter



_context = {}

_Sequence = namedtuple('_Sequence', ('name', 'number', 'required'))
_TableWithData = namedtuple('_TableWithData', ('name', 'number', 'required'))
_Table = namedtuple('_Table', ('name', 'required'))

_BACKUPDIR = './sd_backup'
_FILE_PREFIX = 'sd_backup'
_FILEIDS = ('_1_data', '_2_schemaonly')
_TABLES = {
    'data': [
        _Sequence(name='zoning_rules_code_seq', number=410, required=False),
        _TableWithData(name='construction_commodities', number=10, required=True),
        _TableWithData(name='current_year_table', number=20, required=True),
        _TableWithData(name='cut_parcels', number=21, required=False),
        _TableWithData(name='cuts_and_remnants', number=22, required=False),
        _TableWithData(name='density_step_points', number=30, required=True),
        _TableWithData(name='development_fee_schedules', number=40, required=True),
        _TableWithData(name='development_fees', number=50, required=True),
        _TableWithData(name='exchange_results', number=60, required=True),
        _TableWithData(name='local_effect_distances', number=70, required=True),
        _TableWithData(name='local_effect_parameters', number=80, required=True),
        _TableWithData(name='local_effects', number=90, required=True),
        _TableWithData(name='luzs', number=100, required=True),
        _TableWithData(name='parcel_cost_xref', number=110, required=True),
        _TableWithData(name='parcel_fee_xref', number=120, required=True),
        _TableWithData(name='parcel_zoning_xref', number=130, required=True),
        _TableWithData(name='parcels', number=140, required=True),
        _TableWithData(name='parcels_backup', number=150, required=True),
        _TableWithData(name='parcels_backup_with_geom', number=160, required=True),
        _TableWithData(name='phasing_plan_current', number=170, required=False),
        _TableWithData(name='phasing_plan_xref', number=180, required=False),
        _TableWithData(name='phasing_plans', number=190, required=False),
        _TableWithData(name='random_seeds', number=200, required=True),
        _TableWithData(name='redevelopment_control', number=210, required=False),
        _TableWithData(name='sitespec_parcels', number=220, required=True),
        _TableWithData(name='sitespec_site_amounts', number=230, required=True),
        _TableWithData(name='sitespec_totals', number=240, required=True),
        _TableWithData(name='space_taz_limits', number=250, required=True),
        _TableWithData(name='space_to_commodity', number=260, required=True),
        _TableWithData(name='space_types_group', number=270, required=True),
        _TableWithData(name='space_types_i', number=280, required=True),
        _TableWithData(name='taz_group_constants', number=290, required=True),
        _TableWithData(name='taz_group_space_constants', number=300, required=True),
        _TableWithData(name='taz_groups', number=310, required=True),
        _TableWithData(name='taz_limit_groups', number=320, required=True),
        _TableWithData(name='taz_limit_space_types', number=330, required=True),
        _TableWithData(name='tazs', number=340, required=True),
        _TableWithData(name='tazs_by_taz_group', number=350, required=True),
        _TableWithData(name='transition_constants_i', number=360, required=True),
        _TableWithData(name='transition_cost_codes', number=370, required=True),
        _TableWithData(name='transition_costs', number=380, required=True),
        _TableWithData(name='zoning_permissions', number=390, required=True),
        _TableWithData(name='zoning_rules_i', number=400, required=True),
        _TableWithData(name='zy_floorspace_sd_base', number=420, required=False),
    ],
    'schemaonly': [
        _Table(name='development_amounts_by_event', required=False),
        _Table(name='development_amounts_by_effect', required=False),
        _Table(name='development_events', required=True),
        _Table(name='development_events_history', required=True),
        _Table(name='development_history_summary', required=True),
        _Table(name='floorspacei_view', required=True),
        _Table(name='floorspacei_view2', required=False),
        _Table(name='floorspacei_view_raw', required=False),
        _Table(name='most_recent_cost_year', required=True),
        _Table(name='most_recent_fee_year', required=True),
        _Table(name='local_effect_distances_most_recent', required=False),
        _Table(name='most_recent_local_effect_with_taz', required=False),
        _Table(name='most_recent_local_effect_year', required=False),
        _Table(name='most_recent_taz_limit_year', required=True),
        _Table(name='most_recent_zoning_year', required=True),
        _Table(name='parcels_random_number', required=True),
        _Table(name='parcel_histories', required=False),
        _Table(name='parcels_temp', required=True),
        _Table(name='parcels_view', required=True),
        _Table(name='parcels_with_geom', required=False),
        _Table(name='transition_constants_i_view', required=True)
    ]
}
TABLE_NUMBERS = {td.name: td.number for td in _TABLES['data']}


def _run_threaded(ps, procedure, *args, **kwargs):
    t = Thread(target=procedure, args=((ps,) + args), kwargs=kwargs)
    t.start()
    return t

def _db_get_conn():
    if 'conn' not in _context:
        env = _context['env']
        host = env['PGHOST']
        port = env['PGPORT'] if 'PGPORT' in env else 5432
        user = env['PGUSER']
        password = env['PGPASSWORD']
        database = env['PGDATABASE']

        _context['conn'] = connect(
            database = database,
            host     = host,
            port     = port,
            user     = user,
            password = password
        )

    return _context['conn']

def _db_schema_exists(schema_name=None):
    conn = _db_get_conn()
    with conn, conn.cursor(cursor_factory=DictCursor) as cur:
        qry = (
            'SELECT count(*) AS schema_exists '
            '  FROM information_schema.schemata '
            ' WHERE schema_name = %s;'
        )
        params = [schema_name or _context['schema']]
        cur.execute(qry, params)
        row = cur.fetchone()

    return bool(row['schema_exists'])

def _db_tables_exist(tablenames):
    conn = _db_get_conn()
    with conn, conn.cursor(cursor_factory=DictCursor) as cur:
        qry = (
            'SELECT table_name::text '
            '  FROM information_schema.tables '
            ' WHERE table_schema = %s'
            '   AND table_name IN ({})'
        ).format(', '.join(['%s'] * len(tablenames)))
        params = [_context['schema']] + tablenames
        cur.execute(qry, params)
        rows = cur.fetchall()

    return [row['table_name'] for row in rows]

def _db_move_schema():
    conn = _db_get_conn()
    schema_name = Identifier(_context['schema'])
    backup_name = Identifier("{0[schema]}__backup".format(_context))

    logging.info("Copying old schema %(schema)s to %(schema)s__backup in DB.", _context)
    backup_exists = _db_schema_exists(backup_name.string)

    with conn, conn.cursor() as cur:
        if backup_exists:
            qry = SQL('DROP SCHEMA {} CASCADE;').format(backup_name)
            cur.execute(qry)

        qry = SQL('ALTER SCHEMA {} RENAME TO {};').format(schema_name, backup_name)
        cur.execute(qry)

def _db_create_schema():
    conn = _db_get_conn()
    schema_name = Identifier(_context['schema'])

    with conn, conn.cursor() as cur:
        qry = SQL('CREATE SCHEMA {};').format(schema_name)
        cur.execute(qry)

def _db_clear_schema(move_schema=True):
    conn = _db_get_conn()
    schema_name = Identifier(_context['schema'])

    if move_schema:
        _db_move_schema()

    with conn, conn.cursor() as cur:
        if not move_schema:
            qry = SQL('DROP SCHEMA {} CASCADE;').format(schema_name)
            cur.execute(qry)

        qry = SQL('CREATE SCHEMA {};').format(schema_name)
        cur.execute(qry)

def _db_close_connection():
    if 'conn' in _context:
        _context['conn'].close()
        del _context['conn']

def _chop(s, tail='\n'):
    if s.endswith(tail):
        return s[:-len(tail)]
    return s

def _shell_reader(pipe, print_fn, q, suppress_blanks=False):
    try:
        with pipe:
            for line in iter(pipe.readline, b''):
                line = _chop(line.decode())
                if line.strip() or (not suppress_blanks):
                    q.put((print_fn, line))
    finally:
        q.put(None)


def _is_mingw():
    # MINGW has Linux tools but still leaves platform == 'win32'
    # and has Windows versions of Python modules,
    # so it needs a hybrid approach.
    # Detect it by the entries it adds to PATH.
    return "mingw64" in environ["PATH"]


def _is_windows():
    return platform in ('win32', 'cygwin') and not _is_mingw()


if _is_mingw():
    from posixpath import join as pathjoin
else:
    from os.path import join as pathjoin


def _run_and_log_shell_command(cmd_list, env=None, shell=True, stdout_logger=None, stderr_logger=None):
    loggers = {
        'stdout': stdout_logger or logging.info,
        'stderr': stderr_logger or logging.warning
    }
    # TODO figure out why there are two threads here, one for stdout and one for stderr
    with _run_shell_command(cmd_list, env, shell) as proc:
        q = Queue()
        for (stream, suppress_blanks) in zip(loggers, [False, True]):
            Thread(
                name='T_reader_{}'.format(stream),
                target=_shell_reader,
                args=[getattr(proc, stream), loggers[stream], q, suppress_blanks]
            ).start()

        nthreads = 2
        while nthreads:
            r = q.get()
            if r is None:
                nthreads -= 1
            else:
                (print_fn, msg) = r
                print_fn(msg)

        rv = proc.poll()

    if rv:
        logging.error("%s aborted with error code %d.", cmd_list[0], rv)


def _run_shell_command(cmd_list, env=None, shell=True):
    env = env or _context['env']

    if (not shell) or _is_windows():
        cmd = cmd_list
    elif _is_mingw():
        cmd = "bash -c \"{}\"".format(' '.join(["'{}'".format(cmd_list[0])] + cmd_list[1:]))
    else:
        cmd = [' '.join(cmd_list)]

    return Popen(cmd, shell=shell, env=env, stdout=PIPE, stderr=PIPE)


def _delete_sql_files(targetdir, prefix=None):
    if prefix:
        _to_delete = lambda fn : fn.endswith('.sql') and fn.startswith(prefix)
    else:
        _to_delete = lambda fn : fn.endswith('.sql')

    for filename in listdir(targetdir):
        if _to_delete(filename):
            try:
                remove('{}/{}'.format(targetdir, filename))
            except OSError as e:
                raise SDBackupError(str(e)) from e

def _raise_error(msg, error_class, from_error=None):
    _db_close_connection()
    logging.error("%s: %s!", _context['name'], msg)
    if from_error:
        raise error_class(msg) from from_error
    raise error_class(msg)

def _getenv(ps, use_environ=False):
    env = environ.copy()
    if use_environ:
        if not all([(key in env) for key in ('PGHOST', 'PGUSER', 'PGPASSWORD', 'PGDATABASE')]):
            logging.error("No PSQL environment variables set!")
            raise SDBackupError('No PSQL environment variables set') from OSError()
    else:
        env.update({
            'PGHOST' : ps.sd_host,
            'PGPORT' : str(ps.sd_port),
            'PGUSER' : ps.sd_user,
            'PGPASSWORD' : ps.sd_password,
            'PGDATABASE' : ps.sd_database
        })
    return env

def _get_context(ps, routine, schema, use_environ=False):
    schema = schema or ps.sd_schema
    pgpath = _chop(ps.pgpath, '/')
    backupdir = pathjoin(ps.scendir, getattr(ps, "sd_backup_directory", _BACKUPDIR))
    _context.update({
        'name': routine,
        'schema': schema,
        'backup_cmd': pathjoin(pgpath, 'pg_dump'),
        'restore_cmd': pathjoin(pgpath, 'psql'),
        'back_up_dir': backupdir,
        'dump_base': pathjoin(backupdir, '{}_{}'.format(_FILE_PREFIX, schema)),
        'split_base': pathjoin(backupdir, _FILE_PREFIX),
        'env': _getenv(ps, use_environ)
    })

def _find_missing_tables(tablestocheck):
    missing = []
    for tbl in _db_tables_exist(tablestocheck):
        if tbl not in tablestocheck:
            missing.append(tbl)
    return missing

def _check_for_missing_tables():
    alltables = [t for tables in _TABLES.values() for t in tables]

    required_tables = []
    optional_tables = []
    for tbl in alltables:
        if tbl.required:
            required_tables.append(tbl.name)
        else:
            optional_tables.append(tbl.name)

    missing_req = _find_missing_tables(required_tables)
    if missing_req:
        msg = 'Required table(s) missing from schema: {}'.format(', '.join(missing_req))
        _raise_error(msg, SDBackupError)

    missing_opt = _find_missing_tables(optional_tables)
    if missing_opt:
        msg = 'Optional table(s) missing from schema: {}'.format(', '.join(missing_opt))
        logging.warning('%s: %s', _context['name'], msg)


def _make_join_command(filespec):
    schema = _context['schema'].lower()
    restore_cmd = _context['restore_cmd']

    if _is_windows():
        print_cmd = 'type'
        filespec = filespec.replace('/', '\\')
        replace_cmd = pathjoin(path.dirname(path.abspath(__file__)), 'sd_assert_schema.exe')
        replace_opt = schema
    else:
        print_cmd = 'cat'
        replace_cmd = 'sed'
        replace_opt = "'s/<<schema>>/{}/'".format(schema)

    return [print_cmd, filespec, '|', replace_cmd, replace_opt, '|', restore_cmd, '-q']


def _get_psql_version():
    with _run_shell_command([_context['backup_cmd'], '-V']) as proc:
        version_parts = proc.stdout.read().decode().strip().split()
    for part in version_parts:
        try:
            if float(part):
                return part
        except ValueError:
            continue
    return None


def _do_backup_sd(ps, split=True, overwrite=True, use_environ=False, schema=None):
    _get_context(ps, 'SD database backup', schema, use_environ)
    backupdir = _context["back_up_dir"]

    if path.isdir(backupdir):
        if not access(backupdir, W_OK):
            _raise_error("Backup directory not writeable", SDBackupError, PermissionError())
    else:
        #TODO figure out if checking parcelcut dir makes any sense for some special case
        if not access('../../parcelcut/pecas_db', W_OK):
            _raise_error("Current directory not writeable", SDBackupError, PermissionError())
        try:
            mkdir(backupdir)
        except OSError as e:
            raise SDBackupError(str(e)) from e

    logging.info("Backing up SD database with schema %(schema)s", _context)
    _check_for_missing_tables()

    if path.exists('{0[dump_base]}_1_data.sql'.format(_context)):
        if overwrite:
            _delete_sql_files(backupdir)
        else:
            msg = "Backup files already exist (and overwrite not specified)"
            _raise_error(msg, SDBackupError)

    def mkTableSpec(schema, tbl):
        return '--table="{}"."{}"'.format(schema, tbl.name)

    # This is where we generate & run the command to do the backup.
    #  Once for each file in (
    #       'sd_backup_[schema]_1_data.sql',
    #       'sd_backup_[schema]_2_schemaonly.sql'
    #  )
    for (fileid, opts) in zip(_FILEIDS, ([], ['--schema-only'])):
        filename = '{0[dump_base]}{1}.sql'.format(_context, fileid)
        logging.info("Backing up SD database tables to %s.", filename)
        filespec = '--file={}'.format(filename)
        tableid = fileid.split('_')[-1]
        tablespecs = [mkTableSpec(_context['schema'], t) for t in _TABLES[tableid]]
        cmd = [_context['backup_cmd'], filespec, '--format=p'] + opts + tablespecs
        _run_and_log_shell_command(cmd)

    if split:
        logging.info("Splitting SD backup files.")
        if path.exists('{0[split_base]}_1_data_0000_prologue.sql'.format(_context)):
            if overwrite:
                for fileid in _FILEIDS:
                    _delete_sql_files(backupdir, prefix="{}{}".format(_FILE_PREFIX, fileid))
            else:
                msg = "Split backup files already exist (and overwrite not specified)"
                _raise_error(msg, SDBackupError)

        psql_version = _get_psql_version()

        for fileid in _FILEIDS:
            backup_file = '{}{}.sql'.format(_context['dump_base'], fileid)
            Splitter(
                prefix=_FILE_PREFIX + fileid,
                scenario=_context['schema'],
                table_numbers=TABLE_NUMBERS,
                psqlversion=psql_version
            ).split(backup_file)
            if path.exists('{0[split_base]}{1}_0000_prologue.sql'.format(_context, fileid)):
                logging.info("Removing backup file.")
                remove(backup_file)
            else:
                msg = "Split unsuccessful. Please check and split backup file manually."
                logging.warning('%s: %s', _context['name'], msg)

    return None

def _do_restore_sd(ps, combine=True, overwrite=True, use_environ=False, schema=None):
    _get_context(ps, 'SD database restore', schema, use_environ)

    if not path.isdir(_context["back_up_dir"]):
        _raise_error("SD Backup folder not found", SDRestoreError, FileNotFoundError())

    if combine:
        _update_filenames()
        filename_pattern = '{0[split_base]}{{}}_0000_prologue.sql'.format(_context)
    else:
        filename_pattern = '{0[dump_base]}{{}}.sql'.format(_context)

    if not all([path.exists(filename_pattern.format(fid)) for fid in _FILEIDS]):
        _raise_error("SD Backup files not found", SDRestoreError, FileNotFoundError())

    try:
        if _db_schema_exists():
            if overwrite:
                logging.info("Schema %(schema)s already exists in DB; removing.", _context)
                _db_clear_schema(move_schema=True)
            else:
                _raise_error("Schema already exists in database", SDRestoreError)
        else:
            _db_create_schema()
    except SQLError as e:
        raise SDRestoreError(str(e)) from e
    finally:
        _db_close_connection()

    # This is where we generate & run the command to do the restore.
    if combine:
        for fileid in _FILEIDS:
            logging.info("Restoring SD database tables specified as *%s.sql.", fileid)
            filespec = '{0[split_base]}{1}_*.sql'.format(_context, fileid)
            cmd = _make_join_command(filespec)
            _run_and_log_shell_command(cmd)
    else:
        for fileid in _FILEIDS:
            filename = '{0[dump_base]}{1}.sql'.format(_context, fileid)
            logging.info("Restoring SD database tables in %s.", filename)
            filespec = '--file="{}"'.format(filename)
            cmd = [_context['restore_cmd'], '-q', filespec]
            _run_and_log_shell_command(cmd)

    _db_vacuum_parcel_relations()
    return None

def _db_vacuum_parcel_relations():
    conn = _db_get_conn()
    schema_name = _context['schema']
    logging.info("Vacuuming and analyzing parcel relations: fees, costs, zoning, parcels, and local effect distances")
    old_isolation_level = conn.isolation_level
    conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    cur = conn.cursor()
    qry = SQL("vacuum analyze {schema}.parcel_cost_xref;".format(schema=schema_name))
    cur.execute(qry)
    qry = SQL("vacuum analyze {schema}.parcel_fee_xref;".format(schema=schema_name))
    cur.execute(qry)
    qry = SQL("vacuum analyze {schema}.parcel_zoning_xref;".format(schema=schema_name))
    cur.execute(qry)
    qry = SQL("vacuum analyze {schema}.local_effect_distances;".format(schema=schema_name))
    cur.execute(qry)
    qry = SQL("vacuum analyze {schema}.parcels;".format(schema=schema_name))
    cur.execute(qry)
    conn.set_isolation_level(old_isolation_level)
    cur.close()


# Changes the old *tabledata* format with sequentially numbered tables to the
# new *data* format with stable numberings.
def _update_filenames():
    back_up_dir = _context["back_up_dir"]
    old_fileid = "_tabledata_"
    new_fileid = "_data_"
    for fname in listdir(back_up_dir):
        if old_fileid in fname:
            number_start_index = fname.index(old_fileid) + len(old_fileid)
            old_number = fname[number_start_index:number_start_index + 4]
            new_number = old_number
            if new_number not in ["0000", "9999"]:
                new_number = str(TABLE_NUMBERS[fname.split(".")[1]]).zfill(4)
            _rename(
                pathjoin(back_up_dir, fname),
                pathjoin(back_up_dir, fname.replace(old_fileid + old_number, new_fileid + new_number))
            )


def _rename(src, dest):
    try:
        svnutil.rename(src, dest)
    except svnutil.SVNError:
        rename(src, dest)


def _do_commit_sd(ps, commit_msg=None):
    _get_context(ps, 'SD backup commit to SVN', None)
    backupdir = _context["back_up_dir"]

    def _to_file(s):
        return '{}/{}'.format(backupdir, s.split('/')[-1])

    logging.info("Committing SD backup to SVN")

    commit_msg = commit_msg or "Adding SD backup files for schema {0[schema]}".format(_context)

    backup_files = glob('{0[split_base]}*.sql'.format(_context))

    try:
        resp = check_output(['svn', 'status', backupdir])
    except CalledProcessError:
        _raise_error("Cannot read committed file list from SVN", SDCommitError)

    unversioned_files = [_to_file(s) for s in resp.decode().splitlines() if s.startswith('?')]
    for filename in backup_files:
        if filename in unversioned_files:
            cmd = ['svn', 'add', filename]
            _run_and_log_shell_command(cmd)

    cmd = ['svn', 'commit', '-m', '"{}"'.format(commit_msg)]
    _run_and_log_shell_command(cmd)

    return None

def _parse_args():
    progname = environ['python_cmd_str'] if 'python_cmd_str' in environ else None

    parser = ArgumentParser(prog=progname, description="Backup or restore SD database")
    subs = parser.add_subparsers(title="commands", dest='command')

    parser_backup = subs.add_parser('backup', help        = "Back up SD database",
                                              description = "Back up SD database",
                                              prog        = progname)
    parser_backup.add_argument('-e','--environ',   action = 'store_true',
                                                   dest   = 'use_environ',
                                                   help   = "Force psql to use existing (i.e.: already set) PG* environment variables")
    parser_backup.add_argument('-s','--split',     action = 'store_true',
                                                   dest   = 'split',
                                                   help   = "Split sql files by table after dumping")
    parser_backup.add_argument('-o','--overwrite', action = 'store_true',
                                                   dest   = 'overwrite',
                                                   help   = "Force overwriting of the backup files if they already exist "
                                                            "(otherwise script will halt in this situation).")
    parser_backup.add_argument('schema',           metavar="SCHEMA",
                                                   type   = str,
                                                   help   = "Name of schema to back up; if omitted, then read from pecas.yml")

    parser_restore = subs.add_parser('restore', help        = "Restore SD database",
                                                description = "Restore SD database",
                                                prog        = progname)
    parser_restore.add_argument('-e','--environ',   action = 'store_true',
                                                    dest   = 'use_environ',
                                                    help   = "Force psql to use existing (i.e.: already set) PG* environment variables")
    parser_restore.add_argument('-c','--compile',   action = 'store_true',
                                                    dest   = 'combine',
                                                    help   = "Compile sql files that were split by table (using pgtricks)")
    parser_restore.add_argument('-o','--overwrite', action = 'store_true',
                                                    dest   = 'overwrite',
                                                    help   = "If the schema already exists in DB, then (truncate and) overwrite. "
                                                             "(otherwise script will halt in this situation.)")
    parser_restore.add_argument('schema',           metavar= "SCHEMA",
                                                    type   = str,
                                                    help   = "Name of schema to restore to; if omitted, then read from pecas.yml")

    subs.add_parser('commit', help        = "Commit SD backup to SVN",
                              description = "Commit SD backup to SVN",
                              prog        = progname)

    return parser.parse_args()


##################################################################################################
# SD_BACKUP_RESTORE EXPORTED NAMES
##################################

class SDBackupError(Exception):
    pass

class SDRestoreError(Exception):
    pass

class SDCommitError(Exception):
    pass


def backup_sd(ps, split=True, overwrite=True, use_environ=False, schema=None, threaded=False):
    if threaded:
        return _run_threaded(ps, _do_backup_sd, split, overwrite, use_environ, schema)

    return _do_backup_sd(ps, split, overwrite, use_environ, schema)


def restore_sd(ps, combine=True, overwrite=True, use_environ=False, schema=None, threaded=False):
    if threaded:
        return _run_threaded(ps, _do_restore_sd, combine, overwrite, use_environ, schema)

    return _do_restore_sd(ps, combine, overwrite, use_environ, schema)


def svn_commit_sd(ps, commit_msg=None, threaded=False, **_):
    if threaded:
        return _run_threaded(ps, _do_commit_sd, commit_msg)

    return _do_commit_sd(ps, commit_msg)

##################################################################################################

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO,    stream=stdout)
    logging.basicConfig(level=logging.WARNING, stream=stderr)
    logging.basicConfig(level=logging.ERROR,   stream=stderr)

    main_ps = pr.load_pecas_settings()
    argv = _parse_args()
    globals()[argv.command + '_sd'](main_ps, **vars(argv))
