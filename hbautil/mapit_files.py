class Sequence:
    def __init__(self, num: int, description: str):
        self.num = num
        self.name = description

    def __repr__(self):
        return "Sequence({!r}, {!r})".format(self.num, self.name)


EAGER_AA = Sequence(0, "EagerAA")
EAGER_SD = Sequence(1, "EagerSD")
LAZY_AA = Sequence(2, "LazyAA")
LAZY_SD = Sequence(3, "LazySD")
EAGER_ED = Sequence(4, "EagerED")
LAZY_EMP = Sequence(9, "LazyEmployment")


class UploadFile:
    def __init__(self, file_id: int, name: str, next_year: bool = False):
        self.id = file_id
        self.name = name
        self.next_year = next_year

    def __repr__(self):
        return "UploadFile({!r}, {!r})".format(self.id, self.name)


#
# These must match up to the entries in "output.aa_output_files"
# Add them there as well.
# See "./tests/loadOutput/README" for an example.
ACTIVITY_SUMMARY = UploadFile(1, "ActivitySummary")
ACTIVITY_LOCATIONS = UploadFile(2, "ActivityLocations")
EXCHANGE_RESULTS = UploadFile(3, "ExchangeResults")
EXCHANGE_RESULTS_TOTALS = UploadFile(4, "ExchangeResultsTotals")
HISTOGRAMS = UploadFile(5, "Histograms")
MAKE_USE = UploadFile(7, "MakeUse")
COMMODITY_Z_UTILITIES = UploadFile(8, "CommodityZUtilities")
FLOORSPACE_I = UploadFile(10, "FloorspaceI")
FLOORSPACE_SD = UploadFile(17, "FloorspaceSD", next_year=True)
GS_SUMMARY = UploadFile(50, "gs_summary")
ZONAL_MAKE_USE = UploadFile(9, "ZonalMakeUse")
ACTIVITY_LOCATIONS_2 = UploadFile(11, "ActivityLocations2")
TECHNOLOGY_CHOICE = UploadFile(20, "TechnologyChoice")
TAZ_DETAILED_USE = UploadFile(40, "TAZDetailedUse")
SD_PRICES = UploadFile(21, "SDPrices")
ACTIVITY_TOTALS_I_ED = UploadFile(70, "ActivityTotalsI_ED")
EMPLOYMENT = UploadFile(60, "Employment")
FLOORSPACEOVERRIDES = UploadFile(80, "FloorspaceOverrides")


mapit_upload_sequences = {
    EAGER_AA: [
        ACTIVITY_SUMMARY,
        ACTIVITY_LOCATIONS,
        EXCHANGE_RESULTS,
        EXCHANGE_RESULTS_TOTALS,
        HISTOGRAMS,
        MAKE_USE,
        COMMODITY_Z_UTILITIES,
        FLOORSPACE_I,
        FLOORSPACEOVERRIDES,
    ],
    EAGER_SD: [
        FLOORSPACE_SD,
        GS_SUMMARY,
    ],
    LAZY_AA: [
        ZONAL_MAKE_USE,
        ACTIVITY_LOCATIONS_2,
        TECHNOLOGY_CHOICE,
        TAZ_DETAILED_USE,
    ],
    LAZY_SD: [
        SD_PRICES,
    ],
    EAGER_ED: [
        ACTIVITY_TOTALS_I_ED,
    ],
    LAZY_EMP: [
        EMPLOYMENT,
    ]
}


mapit_files_by_name = {ufile.name: ufile for seq in mapit_upload_sequences.values() for ufile in seq}
mapit_files_by_number = {ufile.id: ufile for seq in mapit_upload_sequences.values() for ufile in seq}
mapit_sequences_by_name = {seq.name: seq for seq in mapit_upload_sequences}
mapit_sequences_by_number = {seq.num: seq for seq in mapit_upload_sequences}
