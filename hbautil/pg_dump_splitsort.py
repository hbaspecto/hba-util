#!/usr/bin/env python

"""
/******************************************************************************
   Script (heavily) modified from original in `pgtricks` package, by Antti
   Kaihola, found at https://github.com/akaihola/pgtricks.

   Copyright (c) 2010, Antti Kaihola
   All rights reserved.
******************************************************************************/
"""
import collections
from argparse import ArgumentParser
from os import path
from re import compile as compile_re
from sys import version_info

import logging

class Matcher(object):
    def __init__(self):
        self._match = None

    def match(self, pattern, data):
        self._match = pattern.match(data) if pattern else None
        return self._match

    def __getattr__(self, attname):
        if not self._match:
            raise ValueError('Pattern did not match')
        return getattr(self._match, attname)

def argParser():
    parser = ArgumentParser(
        description="Split and sort a Postgres SQL database definition, e.g.: as generated by pg_dump."
    )
    parser.add_argument('-p', '--prefix',   action='store', default='', type=str, metavar="OUTPUT-PREFIX",
                        help="prefix to prepend before output filenames")
    parser.add_argument('-s', '--scenario', action='store', default='', type=str, metavar="SCENARIO",
                        help="scenario name (schema) to replace with a metavar in the split/sorted files")
    parser.add_argument('-r', '--rename',   action='store', default='', type=str, metavar="NEW-SCHEMA",
                        help="rename schema to supplied name")
    parser.add_argument('-d', '--dumpver',  action='store', default='', type=str, metavar="DUMP-VERSION",
                        help="version of pg_dump used to generate sql file (re: CVE-2018-1058)", dest='psqlversion')
    parser.add_argument('--version',        action='version', version="%(prog)s 0.9.2")
    parser.add_argument('filename')

    return parser

def try_float(s):
    if not s or s[0] not in '0123456789.-':
        # optimization
        return s
    try:
        return float(s)
    except ValueError:
        return s


def cmp(a, b):
    if type(a) == type(b):
        ord_a = a
        ord_b = b
    else:
        # I actually don't "care" what order these are in, so long as they're consistent
        #  In practice it means floats get sorted before ints get sorted before strs
        #  which sounds good enough to me.
        ord_a = str(type(a))
        ord_b = str(type(b))

    return (ord_a > ord_b) - (ord_a < ord_b)


def cmp_to_key(mycmp):
    'Convert a cmp= function into a key= function'
    class K(object):
        def __init__(self, obj, *args):
            self.obj = obj
        def __lt__(self, other):
            return mycmp(self.obj, other.obj) < 0
        def __gt__(self, other):
            return mycmp(self.obj, other.obj) > 0
        def __eq__(self, other):
            return mycmp(self.obj, other.obj) == 0
        def __le__(self, other):
            return mycmp(self.obj, other.obj) <= 0
        def __ge__(self, other):
            return mycmp(self.obj, other.obj) >= 0
        def __ne__(self, other):
            return mycmp(self.obj, other.obj) != 0
    return K

def linecomp(l1, l2):
    p1 = l1.split('\t', 1)
    p2 = l2.split('\t', 1)
    result = cmp(try_float(p1[0]), try_float(p2[0]))
    if not result and len(p1) == len(p2) == 2:
        return linecomp(p1[1], p2[1])
    return result


if version_info > (3, 0):
    def sort_list(lst):
        lst.sort(key=cmp_to_key(linecomp))
else:
    def sort_list(lst):
        lst.sort(cmp=linecomp)

def dumpfile_syntax(psqlversion):
    # newer versions of pg_dump output different schema syntax in response to CVE-2018-1058
    #  (See: https://wiki.postgresql.org/wiki/A_Guide_to_CVE-2018-1058:_Protect_Your_Search_Path)

    # format changed as of versions:
    #  9.3.22
    #  9.4.17
    #  9.5:12
    #  9.6.8
    #  10.3
    psqlversion = psqlversion.strip().split()[-1]
    changed_at = {9: {3: {22: True}, 4: {17: True}, 5: {12: True}, 6: {8: True}}, 10: {3: True}}
    version_num = [int(s) for s in psqlversion.split('.')]

    def _chk(version, verif):
        if verif is True:
            return True

        if version[0] in verif:
            return _chk(version[1:], verif[version[0]])
        else:
            return (version[0] > min(verif.keys()))

    if _chk(version_num, changed_at):
        return 'new'
    return 'old'

def strip_chars_from(s, ch, pos):
    n = len(s)
    remove = {'start':s.lstrip, 'end':s.rstrip}[pos]
    s = remove(ch)
    return (s, n - len(s))

prefixdelims = ['-', ' ', '_']
defaultschema = "<<schema>>"
schema_comment_format = "Schema: {};"

SQL_PGCATALOGSETCONFIG = "SELECT pg_catalog.set_config('search_path', '', false);\n"
SQL_SETSCHEMASEARCHPATH = "SET search_path = <<schema>>, pg_catalog;\n"

DUMP_VERSION_RE = compile_re(
    "-- Dumped by pg_dump version (?P<version>.*?)\n$"
)
DATA_COMMENT_RE = compile_re(
    "-- Data for Name: (?P<table>.*?); "
    "Type: TABLE DATA; "
    "Schema: (?P<schema>.*?);"
    "|"
    "-- Name: (?P<sequence>.*?); "
    "Type: SEQUENCE SET; "
    "Schema: (?P<sequence_schema>.*?);"
)
TABLE_COMMENT_RE = compile_re(
    "-- Dependencies: |"
    "-- TOC entry "
)
SEQUENCE_SET_RE = compile_re(
    "-- Name: .+; Type: SEQUENCE SET; Schema: |"
    r"SELECT pg_catalog\.setval\('"
)
TABLE_SET_RE = compile_re(
    "-- Name: .+; Type: TABLE; Schema: |"
    "-- Name: .+; Type: VIEW; Schema: "
)
CONSTRAINT_SET_RE = compile_re(
    "-- Name: .+; Type: CONSTRAINT; Schema: "
)
SEARCH_PATH_RE = compile_re(
    "SET search_path = (?P<schema>.*?), pg_catalog;\n$"
)
COMMENT_SCHEMA_REPL_RE = compile_re(
    "\s*--.*; Schema: (?P<schema>.*?);"
)
BAREWORD_SCHEMA_REPL_RE = compile_re(
    r".*[\s(](?P<schema>.*?)\..+"
)
BAREWORD_SCHEMA_INSERT_RE = compile_re(
    r"\s*CREATE VIEW |"
    r"\s*CREATE TABLE |"
    r"\s*ALTER TABLE |"
    r"\s*FROM |"
    r"\s*COMMENT ON COLUMN |"
    r"\s*CREATE INDEX |"
    r"\s*JOIN |"
    r"\s*ADD CONSTRAINT .* REFERENCES |"
    r"\s*SELECT pg_catalog\.setval|"
    r".*DEFAULT nextval"
)
ALTER_TABLE_RE = compile_re(
    r"\s*ALTER TABLE ONLY "
)
COPY_REs = {
    'old' : compile_re(
                r"COPY .*? \(.*?\) FROM stdin;"
            ),
    'new' : compile_re(
                r"COPY (?P<schema>.*?)\..*? "
                r"\(.*?\) FROM stdin;"
            )
}

class Splitter(object):
    COPY_RE = None
    SCHEMA_PREFIX_RE = None
    SCHEMA_COMMENT_RE = None
    SCHEMA_SEARCH_RE = None
    BAREWORD_SCEN_REPL_RE = None

    prefix = ''
    newschema = ''
    newsearchschema = ''
    newcommentschema = ''
    curschema = None
    filesyntax = None

    copy_lines = None
    view_table_list = False
    schema_written = False

    directory = ''
    output = None
    buf = []

    @property
    def replacement(self):
        return self.newschema or defaultschema

    def set_syntax(self, filesyntax):
        self.filesyntax = filesyntax
        self.COPY_RE = COPY_REs[filesyntax] if filesyntax else None

    def set_schema(self, schema, override=False):
        if self.curschema and not override:
            return (schema == self.curschema)

        self.curschema = schema
        if schema:
            self.SCHEMA_PREFIX_RE = compile_re(r"{}\.".format(schema))
            self.SCHEMA_SEARCH_RE = compile_re("{},".format(schema))
            self.SCHEMA_COMMENT_RE = compile_re(schema_comment_format.format(schema))
            self.BAREWORD_SCEN_REPL_RE = compile_re(r".*[\s(']{}\..+".format(schema))
            self.newsearchschema = "{},".format(self.newschema or schema)
            self.newcommentschema = schema_comment_format.format(self.replacement)

        return bool(schema)

    def insert_scen_tablespec(self, tablespec, withfieldname=False):
        speclen = 3 if withfieldname else 2

        if tablespec == 'stdin':
            return tablespec

        parts = tablespec.split('.')
        if len(parts) == speclen:
            if parts[0] == self.curschema:
                parts[0] = self.replacement
        else:
            parts.insert(0, self.replacement)

        return '.'.join(parts)

    def insert_scen_decorated_tabledef(self, tabledef, withfieldname=False):
        (tabledef, ncommas) = strip_chars_from(tabledef, ',', 'end')
        (tablespec, nparens) = strip_chars_from(tabledef, '(', 'start')
        newtablespec = self.insert_scen_tablespec(tablespec, withfieldname)
        return '{}{}{}'.format('(' * nparens, newtablespec, ',' * ncommas)

    def insert_scen_prefix_str(self, prefix, line, withfieldname=False):
        pos = line.index(prefix)
        offset = len(prefix) + 1
        tokens = line[(pos+offset):].split()
        table = self.insert_scen_tablespec(tokens[0], withfieldname)
        return (' ' * pos) + ' '.join([prefix, table] + tokens[1:])

    def insert_scen_createview(self, line):
        return self.insert_scen_prefix_str('CREATE VIEW', line)

    def insert_scen_createtable(self, line):
        return self.insert_scen_prefix_str('CREATE TABLE', line)

    def insert_scen_altertable(self, line):
        if 'ALTER TABLE ONLY' in line:
            return self.insert_scen_prefix_str('ALTER TABLE ONLY', line)
        else:
            return self.insert_scen_prefix_str('ALTER TABLE', line)

    def insert_scen_queryfrom(self, line):
        self.view_table_list = line.strip().endswith(',')
        pos = line.index('FROM') + 4
        tokens = line[pos:].split()
        newtabledef = self.insert_scen_decorated_tabledef(tokens[0])
        return ' '.join([line[:pos], newtabledef] + tokens[1:])

    def insert_scen_colcomment(self, line):
        return self.insert_scen_prefix_str('COMMENT ON COLUMN', line, withfieldname=True)

    def insert_scen_createindex(self, line):
        (line, nspaces) = strip_chars_from(line, ' ', 'start')

        tokens = line.split()
        tokenid = tokens.index('ON') + 1
        tokens[tokenid] = self.insert_scen_tablespec(tokens[tokenid])
        return (' ' * nspaces) + ' '.join(tokens)

    def insert_scen_queryjoin(self, line):
        self.view_table_list = line.strip().endswith(',')
        return self.insert_scen_prefix_str('JOIN', line)

    def insert_scen_queryrawtable(self, line):
        self.view_table_list = line.strip().endswith(',')
        (line, nspaces) = strip_chars_from(line, ' ', 'start')
        tokens = line.split()
        newtabledef = self.insert_scen_decorated_tabledef(tokens[0])
        return (' ' * nspaces) + ' '.join([newtabledef] + tokens[1:])

    def insert_scen_constraintref(self, line):
        (line, nspaces) = strip_chars_from(line, ' ', 'start')

        tokens = line.split()
        tokenid = tokens.index('REFERENCES') + 1
        table = tokens[tokenid]
        (tablespec, params) = table.split('(', 1)
        newtablespec = self.insert_scen_tablespec(tablespec)
        tokens[tokenid] = '('.join([newtablespec, params])
        return (' ' * nspaces) + ' '.join(tokens)

    def insert_scen_in_quotes(self, line: str):
        return line.replace(f"'{self.curschema}.", f"'{self.replacement}")

    def move_to_epilogue(self):
        if "epilogue" not in self.output.name:
            self.new_output('9999_epilogue.sql')

    def flush(self):
        self.output.writelines(self.buf)
        self.buf[:] = []

    def new_output(self, filename):
        if self.output:
            self.output.close()
        self.output = open(path.join(self.directory, (self.prefix + filename)), 'w', newline="\n")

    def split(self, sql_filepath):
        logging.info("starting split on {}".format(sql_filepath))
        self.directory = path.dirname(sql_filepath)

        try:
            self.new_output('0000_prologue.sql')
            matcher = Matcher()

            insert_sch = self.replacement

            def simple_replace(cur_line, chars_after="."):
                return cur_line.replace(
                    "{}{}".format(self.curschema, chars_after),
                    "{}{}".format(insert_sch, chars_after)
                )

            with open(sql_filepath) as sql_f:
                for line in sql_f:
                    if self.copy_lines is None:
                        if line in ('\n', '--\n'):
                            self.buf.append(line)
                        elif line.startswith(SQL_PGCATALOGSETCONFIG[:42]):
                            logging.debug("found setconfig")
                            self.flush()
                            self.buf.append(line)

                            # TODO: This next line is a stopgap to ensure cross-syntax compatibility, until
                            #       Everyone has moved over to the new syntax type. Then it is strongly advised
                            #       to remove it, to ensure the security enhancements that the new syntax
                            #       type is intended to bring about in the first place.
                            #       Let's say we may wanna check back on this in early-mid 2019 or so.
                            #       See also: later TODO under check if self.schema_written
                            self.buf.append(SQL_SETSCHEMASEARCHPATH)
                            self.schema_written = True
                        elif matcher.match(SEARCH_PATH_RE, line):
                            logging.debug("found set search path... ", end='')
                            if not self.schema_written:
                                logging.debug("writing back")
                                if self.set_schema(matcher.group('schema')):
                                    line = simple_replace(line, chars_after=",")
                                self.buf.extend([
                                    SQL_PGCATALOGSETCONFIG,
                                    self.SCHEMA_SEARCH_RE.sub(self.newsearchschema, line)
                                ])
                                self.flush()
                                self.schema_written = True
                            else:
                                logging.debug("skipping")
                        elif TABLE_COMMENT_RE.match(line):
                            pass
                            # Assuming/hoping it's safe to just drop these lines.
                            # self.buf.append(line)
                        else:
                            starting_constraints = False
                            if self.view_table_list:
                                # We're in the middle of the list of tables to include in a view.
                                #  Will need to insert (prepend) the schema name to each table name
                                #  in that list, if it's not yet there.
                                line = self.insert_scen_queryrawtable(line).rstrip() + '\n'
                            elif matcher.match(DUMP_VERSION_RE, line):
                                if not self.filesyntax:
                                    self.set_syntax(dumpfile_syntax(matcher.group('version')))
                            elif matcher.match(DATA_COMMENT_RE, line):
                                if self.set_schema(matcher.group('schema')):
                                    line = self.SCHEMA_COMMENT_RE.sub(self.newcommentschema, line)
                                if not self.schema_written:
                                    # TODO: Remove this clause at some point (see above TODO re: stopgap to
                                    #       ensure cross-syntax compatibility).
                                    self.buf.extend([SQL_PGCATALOGSETCONFIG, SQL_SETSCHEMASEARCHPATH])
                                    self.flush()
                                    logging.debug("schema not written - writing setconfig/setsearchpath")
                                    self.schema_written = True
                                logging.debug("opening new file")
                                table_name = matcher.group('table') or matcher.group('sequence')
                                table_number = self.getTableNumber(table_name)

                                logging.debug("new file #{}".format(table_number))
                                print("Counter",
                                      table_number,
                                      "schema",
                                      self.newschema or 'scen',
                                      'table',
                                      table_name)
                                self.new_output(
                                    '{counter:04}_{schema}.{table}.sql'.format(
                                        counter=table_number,
                                        schema=self.newschema or 'scen',
                                        table=table_name
                                    )
                                )
                            elif matcher.match(self.COPY_RE, line):
                                if 'schema' in matcher.groupdict():
                                    if self.set_schema(matcher.group('schema')):
                                        line = simple_replace(line)
                                else:
                                    line = "COPY {}.{}".format(insert_sch, line[5:])
                                self.copy_lines = []
                            elif matcher.match(COMMENT_SCHEMA_REPL_RE, line):
                                if self.set_schema(matcher.group('schema')):
                                    line = simple_replace(line, chars_after=";")
                                if matcher.match(CONSTRAINT_SET_RE, line):
                                    starting_constraints = True
                            elif self.filesyntax == 'old' and BAREWORD_SCHEMA_INSERT_RE.match(line):
                                def insertscen(x): return x

                                if 'CREATE VIEW' in line:
                                    insertscen = self.insert_scen_createview
                                elif 'CREATE TABLE' in line:
                                    insertscen = self.insert_scen_createtable
                                elif 'ALTER TABLE' in line:
                                    insertscen = self.insert_scen_altertable
                                elif 'COMMENT ON COLUMN' in line:
                                    insertscen = self.insert_scen_colcomment
                                elif 'CREATE INDEX' in line:
                                    insertscen = self.insert_scen_createindex
                                elif 'ADD CONSTRAINT' in line and 'REFERENCES' in line:
                                    insertscen = self.insert_scen_constraintref
                                elif line.strip().startswith('FROM'):
                                    insertscen = self.insert_scen_queryfrom
                                elif line.strip().startswith('JOIN'):
                                    insertscen = self.insert_scen_queryjoin
                                elif line.strip().startswith('SELECT pg_catalog.setval'):
                                    insertscen = self.insert_scen_in_quotes
                                elif 'DEFAULT nextval' in line:
                                    insertscen = self.insert_scen_in_quotes

                                line = insertscen(line).rstrip() + '\n'
                            elif matcher.match(ALTER_TABLE_RE, line):
                                line = simple_replace(line)
                                self.move_to_epilogue()
                            elif SEQUENCE_SET_RE.match(line):
                                line = simple_replace(line)
                            elif self.BAREWORD_SCEN_REPL_RE.match(line):
                                # TODO: Not entirely convinced we won't get any false positives out
                                #       of this one. Needs to be fixed. Unfortunately the fix is to
                                #       implement a full-on SQL parser, which: nope. So... I dunno.
                                #          import crossed_fingers ?
                                line = simple_replace(line)

                            # Line has been processed; is now ok to write.
                            self.buf.append(line)
                            if not starting_constraints:
                                self.flush()
                    else:
                        # This gets run if we're inside a table data block (COPY ... FROM stdin).
                        #  In this case, we don't want to do any processing of the lines themselves,
                        #  just buffer, sort, then dump 'em straight back until we see the "\." line
                        #  indicating end of data.
                        if line == '\\.\n':
                            sort_list(self.copy_lines)
                            self.buf.extend(self.copy_lines)
                            self.buf.append(line)
                            self.flush()
                            self.copy_lines = None
                        else:
                            self.copy_lines.append(line)
        finally:
            if self.output:
                self.output.close()

    def getTableNumber(self, tableName : str) -> str:
        tableNumber = self.table_numbers.get(tableName)
        if tableNumber:
            return tableNumber
        tableNumber = self.currentTableNumber
        self.table_numbers[tableName] = self.currentTableNumber
        self.currentTableNumber += 10
        return tableNumber
       

    def __init__(self, prefix=None, scenario=None, table_numbers=None, rename='', psqlversion=None, **_):
        self.newschema = rename.lower()
        if scenario:
            self.set_schema(scenario.lower())

        if prefix and (prefix[-1] not in prefixdelims):
            prefix += '_'
        self.prefix = prefix


        self.currentTableNumber = 10
        self.table_numbers = table_numbers or dict()

        if psqlversion:
            filesyntax = dumpfile_syntax(psqlversion)
        else:
            filesyntax = None

        self.set_syntax(filesyntax)


if __name__ == '__main__':
    args = argParser().parse_args()

    Splitter(**vars(args)).split(args.filename)
