import logging
import os
from base64 import encodebytes, urlsafe_b64encode
from datetime import datetime
from html import unescape
from json import loads as loadjson
from os.path import join as pathjoin
from urllib.error import HTTPError
from urllib.parse import urlencode
from urllib.request import urlopen, Request as HttpRequest
from zlib import compress as compressz
import csv

from hbautil import mapit_files, settings
from hbautil.pecassetup import mapit_querier, connect_to_mapit
from hbautil.sqlutil import Querier


def get_mapit_https_auth_header(ps):
    if not ps.mapit_httpsuser:
        return None

    authstr = '{}:{}'.format(ps.mapit_httpsuser, ps.mapit_httpspass)
    return 'Basic {}'.format(encodebytes(authstr.encode('ascii'))[:-1].decode("utf-8"))


def log_basic_api_error(context_action, errmsg):
    logging.error("Unable to {} MapIt server: API returned {!r}".format(context_action, errmsg or 'fail'))


class HttpStatus:
    UNAUTHORIZED = 401
    INTERNALERROR = 500
    GATEWAYTIMEOUT = 504


def log_detailed_api_failure(err_code, request_obj, exception_obj):
    try:
        request_data = request_obj.get_data()
    except AttributeError:
        request_data = request_obj.data

    if getattr(request_data, 'decode', None):
        request_data = request_data.decode()

    request_fields = request_data.split('&')
    request_data_print = '&'.join([d for d in request_fields if not d.startswith('file_contents')])

    timestamp = datetime.now().strftime('%Y%m%d%H%M%S.%f')
    debug_filename = "http_err{}_debug_dump_{}.html".format(err_code, timestamp)

    server_response = None
    if err_code == HttpStatus.INTERNALERROR:
        server_response = exception_obj.read().decode()
        server_exc_flag = '<pre class="exception_value">'
        try:
            msg_start = server_response.index(server_exc_flag) + len(server_exc_flag)
            msg_end = server_response.index('</pre>', msg_start)
            if msg_start == msg_end:
                raise ValueError
            server_errmsg = unescape(server_response[msg_start:msg_end])
        except ValueError:
            server_errmsg = "<Could not parse server response>"
    else:
        server_errmsg = None

    logging.error("\n".join(filter(bool, [
        "MapIt API HTTPError {} ({}):".format(err_code, getattr(exception_obj, 'msg', '')),
        "    URL:\n        {}".format(request_obj.get_full_url()),
        "    Data:\n        {}".format(request_data_print),
        "    Headers:",
        "\n".join(["        {}:{}".format(k, v) for (k, v) in request_obj.header_items()]),
        "    Server message:\n        {}".format(server_errmsg) if server_errmsg else "",
        "    ...writing debug output to file {!r}".format(debug_filename)
    ])))

    with open(debug_filename, 'w') as f:
        f.write(str(server_response))


def make_mapit_api_call(ps, apiname, retries=None, **api_params):
    timeout_retries = 3 if retries is None else retries

    headers = {
        'User-Agent': 'PECAS-Routines-Script',
        'Accept': 'application/json'
    }

    url = '/'.join([ps.mapit_httpspath, apiname, ''])

    api_params['accept'] = 'application/json'
    params = urlencode(api_params, True).encode('utf-8')
    req = HttpRequest(url, params, headers)

    # logging.debug("Calling MapIt API {!r}".format(apiname))

    ok = False
    auth_retry = False
    while not ok:
        try:
            resp = urlopen(req).read()
            ok = True
        except HTTPError as e:
            err_code = getattr(e, 'code', None)

            if (err_code == HttpStatus.UNAUTHORIZED) and not auth_retry:
                authline = e.headers['www-authenticate']
                if ':' in authline:
                    authline = authline.split(':')[1]
                scheme = authline.split()[0]
                if scheme.lower() != 'basic':
                    logging.error("MapIt API: Trying to authenticate HTTPS but expecting {!r}.".format(authline))
                    raise

                req.add_header("Authorization", get_mapit_https_auth_header(ps))
                auth_retry = True
                continue

            elif (err_code == HttpStatus.GATEWAYTIMEOUT) and timeout_retries:
                timeout_retries -= 1
                continue

            # noinspection PyTypeChecker
            log_detailed_api_failure(err_code, req, e)
            raise

    # noinspection PyUnboundLocalVariable
    return loadjson(resp)


def get_mapit_interface(ps, iface=None):
    iface = iface or ps.load_output_to_mapit

    if iface is True:
        iface = 'sql'

    return iface


def load_outputs_for_year(ps, year, sequence, excl=None, iface=None):
    if year < ps.mapit_startyear:
        logging.error(
            "Can't load year "+str(year)+" because it's before the mapit startyear"
        )
        return
    iface = get_mapit_interface(ps, iface)
    if iface is False:
        logging.error(
            "Can't load because there's no mapit interface defined"
        )
        return

    if isinstance(iface, str):
        if iface.lower() == 'http':
            return load_outputs_for_year_http(ps, year, sequence, excl)
        elif iface.lower() == 'sql':
            return load_outputs_for_year_sql(ps, year, sequence, excl)

    raise TypeError("Interface {} not supported for load_outputs_for_year().".format(iface))


def load_outputs_for_year_http(ps, year, sequence, excl=None):
    mf = with_mapit_files(ps)
    operation = "loading data sequence {} for year:{}".format(sequence, year)
    logging.info(operation.capitalize())

    compress = True

    excluded_files = [fn if fn.endswith('.csv') else (fn + '.csv') for fn in (excl or [])]

    folder_path = pathjoin(ps.scendir, str(year))
    next_year_folder_path = pathjoin(ps.scendir, str(year + 1))

    if not getattr(ps, 'mapit_httpspath', None):
        logging.error(
            "Field 'mapit_httpspath' required in machine.yml but was not found. "
            "Cannot load outputs to MapIt server."
        )
        return

    # The upload starts to intermittently fail (with a difficult-to-diagnose
    #  HTTP 500 error) if the HTTP POST data string is larger than 128MB (this
    #  size is actually dependent on various server parameters, but in practice,
    #  this is where we're at right now). That generally translates into about
    #  1.6 million rows of CSV data - but I'll make the cutoff 50,000 just to
    #  be on the safe side, especially since that number goes down once you
    #  introduce network delays and the like. Anything larger than that, and I
    #  want to chunk the file into blocks smaller than that cutoff. The API at
    #  the MapIt server end is written such that it can just be re-called
    #  repeatedly with any additional data.
    # If you start seeing HTTP 500 errors in the uploadFileContents API that
    #  you can't pin down, and haven't (obviously) changed anything else, and
    #  other API calls continue to work, this may be the culprit. Try making
    #  chunksz smaller, and see if that helps. :-)
    chunksz = 50000

    resp = make_mapit_api_call(ps, 'getOutputFiles')
    if 'fail' in resp:
        log_basic_api_error(context_action="read file list from", errmsg=resp['fail'])
        return

    is_last_year = (year == ps.stopyear)
    ids = [uf.id for uf in mf.mapit_upload_sequences[sequence]]

    for row in resp['success']:
        # list of rows where each row is a dict with fields 'file_id' and 'csv_file_name'
        file_id = row['file_id']
        if file_id not in ids:
            continue

        uf = mf.mapit_files_by_number[file_id]

        csv_file_name = row['csv_file_name']

        if is_last_year and '_sd_' in row['all_table_name']:
            continue

        if csv_file_name in excluded_files:
            logging.info("Skipping file {}".format(csv_file_name))
            continue

        logging.info("Loading file {}".format(csv_file_name))
        filename = pathjoin(next_year_folder_path if uf.next_year else folder_path, csv_file_name)
        try:
            with open(filename, 'r') as csv_file:
                file_contents = csv_file.readlines()
        except IOError:
            logging.error("NOTICE: No such file or directory:" + filename)
        else:
            counter = 0
            all_table_name = None

            totalsz = len(file_contents)
            for i in range(1, totalsz, chunksz):
                filechunk = ''.join(file_contents[i:i + chunksz]).encode('utf-8')
                if compress:
                    filechunk = urlsafe_b64encode(compressz(filechunk))

                logging.debug("{}: sending rows {}+ of {}".format(csv_file_name, i, totalsz))
                resp = make_mapit_api_call(
                    apiname='uploadFileContents',
                    ps=ps,
                    file_id=file_id,
                    scenario=ps.scenario,
                    year_run=year,
                    file_contents=filechunk,
                    compressed=compress,
                    chunk_num=(i // chunksz),
                    skip_update=(i + chunksz < totalsz)  # Only run update_loaded_scenarios() on the last chunk
                )
                if 'fail' in resp:
                    log_basic_api_error(context_action="upload file contents to", errmsg=resp['fail'])
                    return

                counter += resp['success']['records']
                all_table_name = resp['success']['tblname']

            logging.info("{} record(s) added to {}".format(counter, all_table_name))

    logging.info("Finished " + operation)

FIELDNAME_OVERRIDES = {
    'CommodityZUtilities.csv': {
        'Zone': 'zonenumber'
    },
    'FloorspaceI.csv': {
        'Commodity': 'aa_commodity'
    },
    'TechnologyChoice.csv': {
        'Option': 'option_name',
        'Zone': 'zonenumber'
    },
    'gs_summary.csv': {
        'SpaceTypes': 'space_type_filter',
        'Luz': 'geographic_filter'
    },
    'FloorspaceSD.csv': {
        'Commodity': 'aa_commodity'
    }
}

def map_table_names_to_csv_headers(table_name: str, csv_file_name: str, tr: Querier):
    csv_base_name = os.path.basename(csv_file_name)
    #logging.info("CSV Base Name "+str(csv_base_name))
    overrides = FIELDNAME_OVERRIDES.get(csv_base_name, dict())
    if len(overrides) > 0:
        logging.info("column name overrides: " + str(overrides))
    results = []
    all_col_defs = tr.query(
        "SELECT column_name, data_type, character_maximum_length "
        "from INFORMATION_SCHEMA.COLUMNS "
        "where TABLE_NAME=%(tmp)s AND TABLE_SCHEMA = %(sch)s "
        "order by ordinal_position;",
        tmp=table_name
    )
    print("Column defs are "+str(all_col_defs))
    with open(csv_file_name, "r") as open_file:
        reader = csv.DictReader(open_file)
        #line1 = next(reader)  # maybe we need to read a line to get the fieldnames
        for column in reader.fieldnames:
            col_strip: str = column.lower().replace('_', '')
            found = False
            for db_column in all_col_defs:
                db_col_strip = db_column.column_name.lower().replace('_', '')
                if db_col_strip == col_strip or db_column.column_name == overrides.get(column):
                    results.append([column, db_column.column_name, db_column.data_type, db_column.character_maximum_length])
                    found = True
                    break
            if not found:
                logging.warning("Can't find MapIt column for CSV column {}".format(str(column)))
                results.append([column, '_unmatched_' + column.lower(), 'character varying', None])
    return results


def load_outputs_for_year_sql(ps, year, sequence, excl=None):
    mf = with_mapit_files(ps)
    logging.info("Loading data sequence " + str(sequence) + " for year:" + str(year))

    excluded_files = [fn if fn.endswith('.csv') else (fn + '.csv') for fn in (excl or [])]

    folder_path = os.path.join(ps.scendir, str(year))
    next_year_folder_path = os.path.join(ps.scendir, str(year + 1))

    filesrows = file_rows_for_sequence(ps, sequence)

    q = mapit_querier(ps)
    for row in filesrows:  # for each output file
        uf = mf.mapit_files_by_number[row.file_id]

        csv_file_name = row.csv_file_name
        if csv_file_name in excluded_files:
            logging.info("Skipping file {}".format(csv_file_name))
            continue

        logging.info("Loading file " + csv_file_name)
        all_table_name = row.all_table_name
        temp_table_name = row.temp_table_name

        if hasattr(row, "partitioned"):
            partition = row.partitioned
        else:
            partition = False

        with q.transaction(named_tuples=True) as tr:
            csv_file = os.path.join(next_year_folder_path if uf.next_year else folder_path, csv_file_name)
            try:
                table_col_list = map_table_names_to_csv_headers(all_table_name, csv_file, tr)
                coldefs = ",\n".join(
                    f"{col[1]} {col[2]}"
                    + ("" if col[3] is None else f"({col[3]})")
                    for col in table_col_list
                )

                tr.query('create temporary table {tmp} (\n{coldefs}\n)', tmp=temp_table_name, coldefs=coldefs)

                tr.load_from_csv("{tmp}", csv_file, tmp=temp_table_name, use_tempfile=True)

                counter = tr.query('SELECT count(*) FROM {tmp};', tmp=temp_table_name)[0][0]

                # now insert the records from the temporary table into the full table which contains data for each
                # year/scenario. This needs to happen before the scenario runs
                if partition:
                    logging.info("Trying to partition " + all_table_name)
                    tr.query(
                        "select * from {sch}.{all}__create_partition(%(scen)s)",
                        all=all_table_name, scen=ps.scenario
                    )
                ## the columns to insert
                matched_cols = []
                for colitem in table_col_list:
                    if colitem[1].startswith("_unmatched_"):
                        continue
                    matched_cols.append(colitem)

                cols = ", ".join([col[1] for col in matched_cols])
                tr.query(
                    "INSERT INTO {sch}.{all} (scenario, year_run, {cols}) "
                    "SELECT %(scen)s, %(year)s, {cols} from {tmp}",
                    cols=cols, scen=ps.scenario, year=year, tmp=temp_table_name, all=all_table_name
                )

                logging.info("%d record(s) added to %s" % (counter, all_table_name))
            except IOError:  # if file does not exist. ex. ActivityLocations2_6k.csv not created every year.
                logging.warning("NOTICE: No such file or directory:" + csv_file)

    with q.transaction() as tr:
        tr.query('SET search_path = {sch}')
        tr.callproc('output.update_loaded_scenarios', [ps.scenario, year, year])
        logging.info("Finished loading data sequence " + str(sequence) + " for year:" + str(year))


def file_rows_for_sequence(ps, sequence):
    mf = with_mapit_files(ps)
    q = mapit_querier(ps)
    with q.transaction(named_tuples=True) as tr:
        return tr.query(
            'SELECT * FROM {sch}.aa_output_files where file_id in %(ids)s;',
            ids=tuple(uf.id for uf in mf.mapit_upload_sequences[sequence])
        )


def clear_upload(ps, year, iface=None):
    iface = get_mapit_interface(ps, iface)
    if iface is False:
        return

    if isinstance(iface, str):
        if iface.lower() == 'http':
            return clear_upload_http(ps, year)
        elif iface.lower() == 'sql':
            return clear_upload_sql(ps, year)

    raise TypeError("Interface {} not supported for clear_upload().".format(iface))


def clear_upload_http(ps, year):
    operation = "clearing outputs for year:{}".format(year)
    logging.info(operation.capitalize())

    if not getattr(ps, 'mapit_httpspath', None):
        logging.error(
            "Field 'mapit_httpspath' required in machine.yml but was not found. "
            "Cannot load outputs to MapIt server."
        )
        return

    resp = make_mapit_api_call(
        ps,
        apiname="clearOutputs",
        schema=ps.mapit_schema,
        scenario=ps.scenario,
        years=year,
        removeResults=True,
        removeABM=False
    )
    if 'fail' in resp:
        log_basic_api_error(context_action="clear upload from", errmsg=resp['fail'])
        return

    logging.info("Finished " + operation)


def clear_upload_sql(ps, year):
    mapit_querier(ps).query(
        "set search_path={sch}; select "
        "clean_up_tables_for_scenario_and_year("
        "%(scen)s, %(year)s)",
        scen=ps.scenario, year=year
    )


def clear_specific_uploads(ps, years, sequences, iface=None):
    iface = get_mapit_interface(ps, iface)
    if iface is False:
        return

    if isinstance(iface, str):
        if iface.lower() == 'sql':
            return clear_specific_uploads_sql(ps, years, sequences)

    raise TypeError("Interface {} not supported for clear_upload().".format(iface))


def clear_specific_uploads_sql(ps, years, sequences):
    # TODO We need to delete from the underlying _widx table for all_taz_detailed_use

    q = mapit_querier(ps)

    for sequence in sequences:
        logging.info("Deleting data sequence {} for years: {}".format(sequence, years))

        filesrows = file_rows_for_sequence(ps, sequence)

        for row in filesrows:
            csv_file_name = row.csv_file_name
            logging.info("Removing file " + csv_file_name)
            table_name = row.underlying_table_name
            if table_name is None:
                table_name = row.all_table_name

            q.query(
                "delete from {sch}.{all} "
                "where scenario = %(scen)s and year_run in %(years)s",
                scen=ps.scenario, years=tuple(years), all=table_name
            )

            table_name = row.upload_table_name
            if table_name is not None:
                q.query(
                    "delete from {sch}.{all} "
                    "where scenario = %(scen)s and year_run in %(years)s",
                    scen=ps.scenario, years=tuple(years), all=table_name
                )


def with_mapit_files(ps):
    mf = settings.from_module(mapit_files)
    return mf.update(ps)
