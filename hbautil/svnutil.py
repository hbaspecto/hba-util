import subprocess


def rename(src, dest):
    try:
        subprocess.run(["svn", "move", src, dest], check=True)
    except subprocess.CalledProcessError:
        raise SVNError()


class SVNError(Exception):
    pass
