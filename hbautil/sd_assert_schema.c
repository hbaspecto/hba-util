#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * Quick and dirty program to re-insert a schema ID into schema-escaped text.
 * Pipes stdin to stdout, replacing any instance of '<<schema>>' with scenario.
 */

/*
 * This is used so that the Python `runpecas.py` (and related) code can re-insert
 *  the schema at the command line when restoring SD database on Windows. The
 *  '<<schema>>' placeholder conflicts with subprocess.{run|Popen} on Windows,
 *  because the latter has no way of appropriately escaping the string when
 *  shell=True, leading the shell to misinterpret the '<' and '>' characters. And
 *  shell needs to be true because `type` is an internal cmd.exe command. Sigh.
 *  (This is not an issue on Unix/MacOS.)
 *
 * It's pretty much all ANSI standard C. A great little free Windows compiler that
 *  you can use to (re-)create the executable when needed (assuming you don't have
 *  a full C development environment) is TCC: https://bellard.org/tcc/
 */

#define BOOL int
#define FALSE 0
#define TRUE !FALSE

#define ERR_BAD_USAGE -1
#define ERR_READ_ERROR -2

int usage( errcode ) {
    FILE *stream = (errcode ? stderr : stdout);
    
    fprintf( stream, "usage: sd_assert_schema.exe scenario\n" );
    fprintf( stream, "       pipes stdin to stdout, replacing any instance" );
    fprintf( stream,        "of '<<schema>>' with scenario.\n" );

    exit( errcode );
}

int sanity_check( int argc, char *argv[] ) {
    if (argc == 2) {
        if ((! strcmp( argv[ 1 ], "-h" )) || (! strcmp( argv[ 1 ], "--help" ))) {
            usage( 0 );
        }
        return 1;
    }
    
    if (argc == 3) {
        if (strcmp( argv[ 1 ], "-s" ) && strcmp( argv[ 1 ], "--schema" )) {
            usage( ERR_BAD_USAGE );
        }
        return 2;
    }
    
    usage( ERR_BAD_USAGE );
}

BOOL check_schema_name( char *schema ) {
    char stype = schema[ 0 ];
    char prefixes[] = "cipstw";
    
    stype = schema[ 0 ];
    if (strchr( prefixes, stype ) == NULL) {
        return FALSE;
    }
    
    /* good enough... */
    return (isdigit( schema[ 1 ] ));
}

void assert_schema( char *schema ) {
    char c;
    
    int i;
    int ptr = 0;
    char template[] = "<<schema>>";
    int maxlen = strlen( template );
    
    int ferr = 0;
    
    while ((c = getchar()) != EOF) {
        if (c == template[ ptr ]) {
            /* checks if we are matching the '<<schema>>' template */
            if (++ptr == maxlen) {
                /* yes, and in fact, we've fully matched it */
                printf( "%s", schema );
                ptr = 0;
            }
        } else {
            if (ptr) {
                /* only a partial match of the template -
                    output up to that point and continue piping */
                for (i = 0; i < ptr; i++) {
                    putchar( template[ i ] );
                }
                ptr = 0;
            }
            putchar( c );
        }
    }

    if (ferr = ferror( stdin )) {
        fprintf( stderr, "Error %d reading stdin.\n", ferr );
        exit( ERR_READ_ERROR );
    }

}

int main( int argc, char *argv[] ) {
    char *schema;
    char msg[ 80 ];
    
    schema = argv[ sanity_check( argc, argv ) ];

    if (! check_schema_name( schema )) {
        sprintf( msg, "Schema name %s does not follow pattern of "
                      "{c,i,p,s,t,w}<num>[<suffix>]\n", schema );
        fprintf( stderr, msg );
        usage( ERR_BAD_USAGE );
    }

    assert_schema( schema );
    
    return 0;
}
