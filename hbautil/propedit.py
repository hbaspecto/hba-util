# Utility module for editing a properties file on the fly.
from typing import List, Dict, Any, Iterator


class Props:
    def __init__(self, props: Dict[str, int], contents: List[str], file_type: str):
        self.props = props
        self.contents = contents
        self.file_type = file_type

    def new_prop(self, prop_name, value) -> str:
        """Sets the named property to the specified value.

        The old value, if any, is returned as a string. The new_value parameter is
        converted to a string using str(). Adds the property if it doesn't exist.
        """
        if prop_name not in self.props:
            self.props[prop_name] = value
            if not self.contents[-1].endswith("\n"):
                self.contents[-1] += "\n"
            self.contents.append(prop_name + "=" + str(value) + "\n")
        else:
            return self.set_prop(prop_name, value)

    def set_prop(self, prop_name: str, new_value: Any) -> str:
        """Sets the named property to the specified value.

        The old value is returned as a string. The new_value parameter is converted
        to a string using str(). Raises KeyError if the property doesn't exist or
        if the file hasn't been loaded yet.
        """
        index = self.props[prop_name]
        line = self.contents[index]
        parts = line.split("=")
        name = parts[0].strip()
        old_value = "=".join(parts[1:]).strip()
        sep = " = " if self.file_type == "py" else "="
        self.contents[index] = name + sep + str(new_value) + "\n"
        return old_value

    def save_props(self, file_name: str) -> None:
        """Saves the modified properties to the specified file.

        All comments and other ignored lines will be written back to the file in their original order.

        Raises an IOError if there is a problem writing the file.
        """
        with open(file_name, "w") as file:
            file.writelines(self.contents)

    def __iter__(self) -> Iterator[str]:
        for prop_name in self.props:
            yield prop_name

    def __contains__(self, prop_name: str) -> bool:
        return prop_name in self.props

    def __getitem__(self, prop_name: str) -> str:
        index = self.props[prop_name]
        line = self.contents[index]
        parts = line.split("=")
        return "=".join(parts[1:]).strip()


def load_props(file_name: str) -> Props:
    """Loads the specified properties file into memory.
    
    Ignores all lines that start with a hash symbol. Lines that do not start with a hash sign but contain no equal sign
    are assumed to be part of a multi-line definition attached to the previous property.

    Raises an IOError if there is a problem reading the file.
    """
    props = {}
    contents = []
    i = 0
    with open(file_name, "r") as file:
        for line in file:
            if line.startswith("#"):
                contents.append(line)
                i += 1
            elif "=" in line:
                parts = line.split("=")
                if len(parts) >= 2:
                    props[parts[0].strip()] = i
                contents.append(line)
                i += 1
            elif len(line.strip()) > 0 and len(contents) > 0:
                contents[-1] += line
            else:
                contents.append(line)
                i += 1
    bits = file_name.split(".")
    file_type = "" if len(bits) == 1 else bits[-1]
    return Props(props, contents, file_type)
