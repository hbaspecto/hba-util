import copy
from collections import deque
from types import ModuleType
from typing import Any, Dict, Callable, List, Tuple, Optional

import hbautil.scriptutil as su

import yaml


class Settings:
    """
    A program's configuration settings. Each setting is an attribute of this object. For example:

    settings = Settings(scenario="W01", num_threads=8, convergence_threshold=0.01)
    print(settings.scenario) # Prints "W01"
    print(settings.num_threads) # Prints "8"
    print(settings.convergence_threshold) # Prints "0.01"
    settings.not_here # Non-existent setting raises AttributeError
    """

    def __init__(self, not_found_msg="Required setting {name} is missing", **settings):
        if settings is None:
            self._settings = {}
        else:
            self._settings = dict(settings)
        self.not_found_msg = not_found_msg

    def __getattr__(self, item):
        try:
            return self._settings[item]
        except KeyError:
            raise AttributeError(self.not_found_msg.format(name=item))

    @property
    def as_dict(self) -> Dict[str, Any]:
        return dict(self._settings)

    def get(self, item, default=None):
        try:
            return self._settings[item]
        except KeyError:
            return default

    @property
    def _constructor_args(self):
        return [], {}

    def clone_with(self, **new_settings):
        """
        Creates a new Settings with the specified settings added or replaced.
        """
        args, kwargs = self._constructor_args
        new_obj = self.__class__(*args, **kwargs)
        for setting_name, value in self._settings.items():
            new_obj._settings[setting_name] = value
        for setting_name, new_value in new_settings.items():
            new_obj._settings[setting_name] = new_value
        return new_obj

    def update(self, other: 'Settings'):
        return self.clone_with(**other.as_dict)

    def __str__(self):
        return "\n".join("{}={}".format(setting_name, value) for setting_name, value in self._settings.items())


def from_python(path: str) -> Settings:
    """
    Loads settings from an old-style Python configuration file. Settings defined as expressions are set to the value
    of those expressions, not the expressions themselves.
    """
    ignore_imports = ["machine_settings", "mapit_settings", "pecas_routines"]

    def ignore(line):
        return any([bad in line for bad in ignore_imports])

    with open(path, 'r') as f:
        settings_dict = {
            "scendir": ".",
            "irange": su.irange,  # Some old-style Python configuration files use irange to build lists
        }
        exec("\n".join([line for line in f if not ignore(line)]), {}, settings_dict)
        settings_dict = {k: v for k, v in settings_dict.items() if not isinstance(v, Callable)}
        return Settings(**settings_dict)


def from_yaml(*paths: str, pattern_lists: List[str] = None) -> Settings:
    """
    Loads settings from one or more YAML files. The resulting ``Settings`` object will contain all settings from
    all provided paths, with settings in later files overriding settings in earlier files with the same name.

    The ``pattern_lists`` argument specifies the names of list settings configured as the start, end, step,
    and additional elements. For example, if ``pattern_lists`` contains ``"spam_years"``, this function will create
    the setting ``spam_years`` by looking instead for settings in the YAML file called ``spam_years_start``,
    ``spam_years_stop``, ``spam_years_interval``, and ``spam_years_extra``. The list will contain all the elements in
    ``spam_years_extra``, plus all the numbers from ``spam_years_start`` to ``spam_years_stop`` inclusive,
    counting by steps of ``spam_years_interval``.
    """
    result = Settings()
    for path in paths:
        with open(path, 'r') as f:
            contents = yaml.safe_load(f) or {}
            if pattern_lists is not None:
                contents = _combine_pattern_lists(contents, pattern_lists)
            result = result.clone_with(**contents)
    return result


def from_module(module) -> Settings:
    """
    Loads settings from a module, making the module's names available as attributes.
    This is useful if you want to override some of the settings without changing them directly in the module.
    """
    settings_dict = {
        k: v for k, v in module.__dict__.items() if
        not k.startswith("_") and
        not isinstance(v, Callable) and
        not isinstance(v, ModuleType)
    }
    return Settings(**settings_dict)


def _combine_pattern_lists(contents: Dict[str, Any], pattern_lists: List[str]) -> Dict[str, Any]:
    result: Dict[str, Any] = dict(contents)
    for pattern_name in pattern_lists:
        if pattern_name in result:
            pattern_value = result[pattern_name]
            if isinstance(pattern_value, dict):
                start = pattern_value.get("start")
                stop = pattern_value.get("stop")
                interval = pattern_value.get("interval", 1)
                extras = pattern_value.get("extra", [])
                main = [] if start is None and stop is None else list(range(start, stop + 1, interval))
                result[pattern_name] = sorted(main + extras)
    return result


class YamlTemplate:
    def __init__(self, contents: Dict[str, Any], comments: Dict[str, List[str]], pattern_lists: List[str]):
        self.contents = contents
        self.comments = comments
        self.pattern_lists = pattern_lists

    def update(self, settings: Settings) -> 'YamlTemplate':
        new_contents = copy.deepcopy(self.contents)
        settings_dict = settings.as_dict
        for key in list(new_contents):
            if key in settings_dict and key not in self.pattern_lists:
                new_contents[key] = settings_dict[key]

        new_contents = _split_pattern_lists(new_contents, settings, self.pattern_lists)

        return YamlTemplate(new_contents, self.comments, self.pattern_lists)

    def __repr__(self):
        return f"YamlTemplate({self.contents!r}, comments={self.comments!r}, pattern_lists={self.pattern_lists!r})"


def _split_pattern_lists(contents: Dict[str, Any], settings: Settings, pattern_lists: List[str]) -> Dict[str, Any]:
    result = copy.deepcopy(contents)
    settings_dict = settings.as_dict
    for pattern_name in pattern_lists:
        if pattern_name in settings_dict:
            start, stop, interval, extras = _decompose_arithmetic(settings_dict[pattern_name])
            if start is None and stop is None:
                result[pattern_name] = extras
            else:
                pattern_value = dict()
                pattern_value["start"] = start
                pattern_value["stop"] = stop
                if interval > 1:
                    pattern_value["interval"] = interval
                if extras:
                    pattern_value["extra"] = extras
                result[pattern_name] = pattern_value
    return result


def _decompose_arithmetic(pattern: List[int]) -> Tuple[Optional[int], Optional[int], Optional[int], List[int]]:
    result = (None, None, None, pattern)

    pattern = sorted(pattern)
    longest_progression = []
    n = len(pattern)
    if n <= 2:
        return result

    dmax = (pattern[-1] - pattern[0]) // (n // 2)
    for search_start in [n // 3, 2 * n // 3]:
        i = search_start
        while i < len(pattern) and pattern[i] <= pattern[search_start] + dmax:
            j = i + 1
            while j < len(pattern) and pattern[j] <= pattern[i] + dmax:
                progression = _extend_progression(pattern, i, j)
                if len(progression) > len(longest_progression):
                    longest_progression = progression
                j += 1
            i += 1

    return (
        longest_progression[0],
        longest_progression[-1],
        longest_progression[1] - longest_progression[0],
        _remove_sublist(pattern, longest_progression)
    )


def _extend_progression(pattern: List[int], i: int, j: int) -> List[int]:
    diff = pattern[j] - pattern[i]
    progression = deque([pattern[i], pattern[j]])

    best_before = pattern[i]
    for b in range(i - 1, -1, -1):
        if pattern[b] == best_before - diff:
            best_before = pattern[b]
            progression.appendleft(best_before)
        elif pattern[b] < best_before - diff:
            break

    best_after = pattern[j]
    for a in range(j + 1, len(pattern)):
        if pattern[a] == best_after + diff:
            best_after = pattern[a]
            progression.append(best_after)
        elif pattern[a] > best_after + diff:
            break

    return list(progression)


def _remove_sublist(big_list: List[int], small_list: List[int]) -> List[int]:
    result = []
    small_i = 0
    for big_i, value in enumerate(big_list):
        if small_list[small_i] == value:
            small_i += 1
            if small_i >= len(small_list):
                result.extend(big_list[(big_i + 1):])
                break
        else:
            result.append(value)

    return result


def template_from_yaml(path: str, pattern_lists: List[str] = None) -> YamlTemplate:
    if pattern_lists is None:
        pattern_lists = []
    with open(path, 'r') as f:
        comments = {}
        cur_comments = []
        yaml_lines = []
        for line in f:
            if line.startswith("#") or not line.strip():
                cur_comments.append(line)
            else:
                yaml_lines.append(line)
                key = line.split(":")[0]
                if cur_comments:
                    comments[key] = cur_comments
                    cur_comments = []

        if cur_comments:
            comments[""] = cur_comments

        contents = yaml.safe_load("".join(yaml_lines))
        return YamlTemplate(contents, comments, pattern_lists)


def template_to_yaml(path: str, template: YamlTemplate):
    with open(path, 'w') as f:
        yaml_lines = yaml.safe_dump(template.contents, sort_keys=False).splitlines(keepends=True)
        all_lines = []
        for line in yaml_lines:
            key = line.split(":")[0]
            if key in template.comments:
                all_lines.extend(template.comments[key])
            all_lines.append(line)
        if "" in template.comments:
            all_lines.extend(template.comments[""])
        f.writelines(all_lines)
