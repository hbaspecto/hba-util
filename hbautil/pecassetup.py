"""Setup functions for PECAS and PECAS-related tools"""
import logging
import os
import socket
from datetime import datetime
from typing import Optional
from os.path import join

import psycopg2

from hbautil import settings
from hbautil.settings import Settings
from hbautil.sqlutil import Querier
from hbautil import scriptutil as su

pecas_settings_pattern_lists = [
    "skimyears",
    "aayears",
    "constrainedyears",
    "tmyears",
    "snapshotyears",
    "find_expected_values_years",
    "travel_model_input_years",
]


def load_pecas_settings(settings_fname="pecas.yml") -> Settings:
    """
    Loads PECAS settings from the specified YAML file.
    Also loads default machine-specific settings from machine.yml,
    and machine-specific settings from machine_<name>.yml, where
    <name> is the lowercased hostname up to the first dot.
    """
    base_settings = settings.from_yaml(
        settings_fname, "machine.yml", local_machine_settings_fname(),
        pattern_lists=pecas_settings_pattern_lists
    )

    setting_overrides = {}
    if not hasattr(base_settings, "scenario") or base_settings.scenario in [None, "guess"]:
        scenario = guess_scenario_name()
        if scenario is None:
            raise ValueError(
                "Couldn't deduce the scenario name from the directory; "
                "please specify it manually using the scenario setting in pecas.yml"
            )
        setting_overrides["scenario"] = scenario
    else:
        scenario = base_settings.scenario
    if not hasattr(base_settings, "sd_schema") or base_settings.sd_schema in [None, "guess"]:
        setting_overrides["sd_schema"] = scenario.lower()

    if base_settings.sd_backupyear is None:
        setting_overrides["sd_backupyear"] = base_settings.baseyear
    if base_settings.mapit_startyear is None:
        setting_overrides["mapit_startyear"] = base_settings.baseyear

    return base_settings.clone_with(**setting_overrides)


def local_machine_settings_fname():
    return "machine_{}.yml".format(machine_name())


def machine_name():
    return socket.gethostname().split(".")[0].lower()


def guess_scenario_name() -> Optional[str]:
    """Take a guess at the current scenario name.
    """
    # If mrsgui set the name, then use it.
    scenario_name = os.environ.get("MRSGUI_SCENARIO_NAME")
    if scenario_name:
        return scenario_name

    # Otherwise, take a guess from the pathname.
    return os.path.basename(os.getcwd())


def connect_to_mapit(ps):
    return psycopg2.connect(
        database=ps.mapit_database,
        host=ps.mapit_host,
        port=ps.mapit_port,
        user=ps.mapit_user,
        password=ps.mapit_password)


def connect_to_mapit_fn(ps):
    def _connect_to_mapit():
        return connect_to_mapit(ps)
    return _connect_to_mapit


def connect_to_sd(ps):
    return psycopg2.connect(
        database=ps.sd_database,
        host=ps.sd_host,
        port=ps.sd_port,
        user=ps.sd_user,
        password=ps.sd_password)


def connect_to_sd_fn(ps):
    def _connect_to_sd():
        return connect_to_sd(ps)
    return _connect_to_sd


def mapit_querier(ps):
    if ps.sql_system == "postgres":
        return Querier(lambda: connect_to_mapit(ps), sch=ps.mapit_schema)
    else:
        # When we need to work with other database systems, we'll implement this
        raise NotImplementedError


def sd_querier(ps, **kwargs):
    if ps.sql_system == "postgres":
        return Querier(lambda: connect_to_sd(ps), sch=ps.sd_schema, **kwargs)
    else:
        # When we need to work with other database systems, we'll implement this
        raise NotImplementedError


def set_up_logging(ps, logger_name: Optional[str] = None, log_file_base_name: Optional[str] = None):
    logging.getLogger(logger_name).setLevel(logging.INFO)
    _add_console_handler(logger_name)
    _add_file_handler(ps, logger_name, log_file_base_name)


def _add_file_handler(ps, logger_name, log_file_base_name):
    file_base = log_file_base_name or "pecas"
    log_fname = join(
        ps.scendir,
        f"{file_base}-{_log_file_suffix(ps)}.log"
    )
    try:
        su.smart_backup(log_fname)
        os.remove(log_fname)
    except IOError:
        pass

    file_handler = logging.FileHandler(log_fname)
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(
        logging.Formatter(
            fmt='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
            datefmt='%y-%m-%d %H:%M:%S',
        )
    )
    logging.getLogger(logger_name).addHandler(file_handler)


def _add_console_handler(logger_name):
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    console_handler.setFormatter(formatter)
    logging.getLogger(logger_name).addHandler(console_handler)


def copy_program_log_file_into_year(ps, program: str, year: int, suffix: str = ""):
    if suffix:
        suffix = "-" + suffix
    su.move_replace(
        join(ps.scendir, "event.log"),
        join(ps.scendir, str(year), f"{program}-{_log_file_suffix(ps)}{suffix}.log")
    )


def _log_file_suffix(ps) -> str:
    return f"{ps.scenario}-{datetime.now().strftime('%Y%m%d-%H%M')}"
