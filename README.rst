hba-util/README
==================================================

Common utilities for HBA's Python projects.  Licensed
under the Apache License, Version 2.0 (see NOTICE.txt)


Links:

- Repo: https://bitbucket.org/hbaspecto/hba-util
- Jenkins: http://jenkins-2.office.hbaspecto.com/job/hba-util
- Docs: http://docs.office.hbaspecto.com/hba-util
- Licence: http://www.hbaspecto.com/products/pecas/software


Quickstart
--------------------------------------------------

This repo is use as a library for other projects.  For now,
edit and update this library as part of another project.
