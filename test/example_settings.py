from os.path import join
# noinspection PyUnresolvedReferences
from machine_settings import *
# noinspection PyUnresolvedReferences
from mapit_settings import *
# noinspection PyUnresolvedReferences
from pecas_routines import *

# This is a comment
foo = 12
bar = "Something"
baz = [
    "This",
    "That",
    # This is also a comment
    "The Other",
]
spam = list(range(foo, foo + 10, 2)) + [15]
ham = join("foo", "bar")
dictionary = {"ab": 25, "xyz": 30}
