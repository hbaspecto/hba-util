
--
-- Name: parcels_temp parcels_temp_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.parcels_temp
    ADD CONSTRAINT parcels_temp_pkey PRIMARY KEY (pecas_parcel_num);


--
-- Name: lef_index_pn; Type: INDEX; Schema: <<schema>>; Owner: postgres
--

CREATE INDEX lef_index_pn ON <<schema>>.local_effect_distances_most_recent USING btree (pecas_parcel_num, local_effect_id);


--
-- Name: randnum_idx; Type: INDEX; Schema: <<schema>>; Owner: postgres
--

CREATE INDEX randnum_idx ON <<schema>>.parcels_temp USING btree (randnum);


--
-- PostgreSQL database dump complete
