

--
-- Data for Name: zoning_permissions; Type: TABLE DATA; Schema: <<schema>>; Owner: postgres
--

COPY <<schema>>.zoning_permissions (zoning_rules_code, space_type_id, min_intensity_permitted, max_intensity_permitted, acknowledged_use, penalty_acknowledged_space, penalty_acknowledged_land, services_requirement, maximum_development_size, minimum_development_size) FROM stdin;
\.
