
--
-- PostgreSQL database dump
--

-- Dumped from database version 10.9 (Ubuntu 10.9-1.pgdg18.04+1)
-- Dumped by pg_dump version 10.15 (Ubuntu 10.15-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET search_path = <<schema>>, pg_catalog;
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: parcel_zoning_xref; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.construction_commodities (
    cc_commodity_id integer NOT NULL,
    cc_commodity_name character varying(150),
    space_types_group_id integer,
    converting_factor double precision
);


ALTER TABLE <<schema>>.construction_commodities OWNER TO postgres;

--
-- Name: current_year_table; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.current_year_table (
    current_year integer NOT NULL
);


ALTER TABLE <<schema>>.current_year_table OWNER TO postgres;

--
-- Name: density_step_points; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.density_step_points (
    space_type_id integer NOT NULL,
    step_point_number integer NOT NULL,
    step_point_intensity double precision,
    slope_adjustment double precision,
    step_point_adjustment double precision
);


ALTER TABLE <<schema>>.density_step_points OWNER TO postgres;

--
-- Name: development_fee_schedules; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.development_fee_schedules (
    fee_schedule_id integer NOT NULL
);


ALTER TABLE <<schema>>.development_fee_schedules OWNER TO postgres;

--
-- Name: development_fees; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.development_fees (
    fee_schedule_id integer NOT NULL,
    space_type_id integer NOT NULL,
    development_fee_per_unit_space_initial double precision,
    development_fee_per_unit_land_initial double precision,
    development_fee_per_unit_space_ongoing double precision,
    development_fee_per_unit_land_ongoing double precision
);


ALTER TABLE <<schema>>.development_fees OWNER TO postgres;

--
-- Name: exchange_results; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.exchange_results (
    commodity character varying NOT NULL,
    luz integer NOT NULL,
    price double precision,
    internal_bought double precision,
    derv double precision
);


ALTER TABLE <<schema>>.exchange_results OWNER TO postgres;

--
-- Name: parcels; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.parcels (
    parcel_id character varying,
    pecas_parcel_num bigint NOT NULL,
    year_built integer,
    taz integer,
    space_type_id integer,
    space_quantity double precision,
    land_area double precision,
    available_services_code integer,
    is_derelict boolean,
    is_brownfield boolean
);


ALTER TABLE <<schema>>.parcels OWNER TO postgres;

--
-- Name: space_to_commodity; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.space_to_commodity (
    space_type_id integer NOT NULL,
    aa_commodity character varying NOT NULL,
    weight real NOT NULL
);


ALTER TABLE <<schema>>.space_to_commodity OWNER TO postgres;

--
-- Name: space_types_i; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.space_types_i (
    space_type_id integer NOT NULL,
    space_type_name character varying(150),
    space_type_code character varying(3),
    age_rent_discount double precision,
    density_rent_discount double precision,
    age_maintenance_cost double precision,
    add_transition_const double precision,
    new_from_transition_const double precision,
    new_to_transition_const double precision,
    renovate_transition_const double precision,
    renovate_derelict_const double precision,
    demolish_transition_const double precision,
    derelict_transition_const double precision,
    no_change_transition_const double precision,
    new_type_dispersion_parameter double precision,
    gy_dispersion_parameter double precision,
    gz_dispersion_parameter double precision,
    gw_dispersion_parameter double precision,
    gk_dispersion_parameter double precision,
    nochange_dispersion_parameter double precision,
    intensity_dispersion_parameter double precision,
    maintenance_cost double precision,
    space_type_group_id integer,
    cost_adjustment_factor double precision,
    construction_capacity_tuning_parameter double precision,
    converting_factor_for_space_type_group double precision,
    min_intensity double precision,
    max_intensity double precision
);


ALTER TABLE <<schema>>.space_types_i OWNER TO postgres;

--
-- Name: local_effect_distances; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.local_effect_distances (
    pecas_parcel_num bigint NOT NULL,
    local_effect_id integer NOT NULL,
    local_effect_distance double precision,
    year_effective integer NOT NULL
);


ALTER TABLE <<schema>>.local_effect_distances OWNER TO postgres;

--
-- Name: local_effect_parameters; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.local_effect_parameters (
    local_effect_id integer NOT NULL,
    space_type_id integer NOT NULL,
    function_type integer,
    max_dist double precision,
    theta_parameter double precision
);


ALTER TABLE <<schema>>.local_effect_parameters OWNER TO postgres;

--
-- Name: local_effects; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.local_effects (
    local_effect_id integer NOT NULL,
    local_effect_name character varying,
    rent_local_effect boolean,
    cost_local_effect boolean
);


ALTER TABLE <<schema>>.local_effects OWNER TO postgres;

--
-- Name: luzs; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.luzs (
    luz_number integer NOT NULL,
    luz_name character varying,
    geom public.geometry(MultiPolygon,3401)
);


ALTER TABLE <<schema>>.luzs OWNER TO postgres;

--
-- Name: parcel_cost_xref; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.parcel_cost_xref (
    pecas_parcel_num bigint NOT NULL,
    cost_schedule_id integer,
    year_effective integer NOT NULL
);


ALTER TABLE <<schema>>.parcel_cost_xref OWNER TO postgres;

--
-- Name: parcel_fee_xref; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.parcel_fee_xref (
    pecas_parcel_num bigint NOT NULL,
    fee_schedule_id integer,
    year_effective integer NOT NULL
);


ALTER TABLE <<schema>>.parcel_fee_xref OWNER TO postgres;

--
-- Name: space_taz_limits; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.space_taz_limits (
    taz_group_id integer NOT NULL,
    taz_limit_group_id integer NOT NULL,
    min_quantity double precision,
    max_quantity double precision,
    year_effective integer NOT NULL
);


ALTER TABLE <<schema>>.space_taz_limits OWNER TO postgres;

--
-- Name: parcel_zoning_xref; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.parcel_zoning_xref (
    pecas_parcel_num bigint NOT NULL,
    zoning_rules_code integer,
    year_effective integer NOT NULL
);


ALTER TABLE <<schema>>.parcel_zoning_xref OWNER TO postgres;

--
-- Name: parcels_backup; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.parcels_backup (
    parcel_id character varying,
    pecas_parcel_num bigint NOT NULL,
    year_built integer,
    taz integer,
    space_type_id integer,
    space_quantity double precision,
    land_area double precision,
    available_services_code integer,
    is_derelict boolean,
    is_brownfield boolean
);


ALTER TABLE <<schema>>.parcels_backup OWNER TO postgres;

--
-- Name: parcels_backup_with_geom; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.parcels_backup_with_geom (
    parcel_id character varying,
    pecas_parcel_num bigint NOT NULL,
    year_built integer,
    taz integer,
    space_type_id integer,
    space_quantity double precision,
    land_area double precision,
    available_services_code integer,
    is_derelict boolean,
    is_brownfield boolean,
    geom public.geometry(MultiPolygon,3401)
);


ALTER TABLE <<schema>>.parcels_backup_with_geom OWNER TO postgres;

--
-- Name: phasing_plan_current; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.phasing_plan_current (
    plan_id integer NOT NULL,
    cur_phase integer
);


ALTER TABLE <<schema>>.phasing_plan_current OWNER TO postgres;

--
-- Name: phasing_plan_xref; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.phasing_plan_xref (
    pecas_parcel_num bigint NOT NULL,
    plan_id integer,
    phase_number integer,
    zoning_rules_code integer
);


ALTER TABLE <<schema>>.phasing_plan_xref OWNER TO postgres;

--
-- Name: phasing_plans; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.phasing_plans (
    plan_id integer NOT NULL,
    initial_release_year integer,
    release_threshold double precision,
    CONSTRAINT phasing_plans_release_threshold_check CHECK (((release_threshold >= (0)::double precision) AND (release_threshold <= (0.7)::double precision)))
);


ALTER TABLE <<schema>>.phasing_plans OWNER TO postgres;

--
-- Name: random_seeds; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.random_seeds (
    pecas_parcel_num bigint NOT NULL,
    year_effective integer NOT NULL,
    seed bigint
);


ALTER TABLE <<schema>>.random_seeds OWNER TO postgres;

--
-- Name: redevelopment_control; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.redevelopment_control (
    space_type_id integer,
    max_addition_intensity double precision,
    min_demolition_age double precision
);


ALTER TABLE <<schema>>.redevelopment_control OWNER TO postgres;

--
-- Name: sitespec_parcels; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.sitespec_parcels (
    pecas_parcel_num bigint NOT NULL,
    siteid integer NOT NULL,
    space_type_id integer NOT NULL,
    space_quantity double precision NOT NULL,
    land_area double precision NOT NULL,
    year_effective integer NOT NULL,
    update_parcel boolean NOT NULL
);


ALTER TABLE <<schema>>.sitespec_parcels OWNER TO postgres;

--
-- Name: sitespec_site_amounts; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.sitespec_site_amounts (
    site_id integer NOT NULL,
    space_type_id integer NOT NULL,
    year integer NOT NULL,
    quantity double precision
);


ALTER TABLE <<schema>>.sitespec_site_amounts OWNER TO postgres;

--
-- Name: COLUMN sitespec_site_amounts.site_id; Type: COMMENT; Schema: <<schema>>; Owner: postgres
--

COMMENT ON COLUMN <<schema>>.sitespec_site_amounts.site_id IS 'Identifier for a future development project';


--
-- Name: COLUMN sitespec_site_amounts.space_type_id; Type: COMMENT; Schema: <<schema>>; Owner: postgres
--

COMMENT ON COLUMN <<schema>>.sitespec_site_amounts.space_type_id IS 'Space Type Id for the development in a project.';


--
-- Name: COLUMN sitespec_site_amounts.year; Type: COMMENT; Schema: <<schema>>; Owner: postgres
--

COMMENT ON COLUMN <<schema>>.sitespec_site_amounts.year IS 'Year of development for a project';


--
-- Name: sitespec_totals; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.sitespec_totals (
    space_type_id integer NOT NULL,
    year_effective integer NOT NULL,
    space_quantity double precision
);


ALTER TABLE <<schema>>.sitespec_totals OWNER TO postgres;

--
-- Name: space_types_group; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.space_types_group (
    space_types_group_id integer NOT NULL,
    space_types_group_name character varying(150),
    cost_adjustment_damping_factor double precision
);


ALTER TABLE <<schema>>.space_types_group OWNER TO postgres;

--
-- Name: taz_group_constants; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.taz_group_constants (
    taz_group_id integer NOT NULL,
    construction_constant double precision
);


ALTER TABLE <<schema>>.taz_group_constants OWNER TO postgres;

--
-- Name: taz_group_space_constants; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.taz_group_space_constants (
    taz_group_id integer NOT NULL,
    space_type_id integer NOT NULL,
    construction_constant double precision
);


ALTER TABLE <<schema>>.taz_group_space_constants OWNER TO postgres;

--
-- Name: taz_groups; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.taz_groups (
    taz_group_id integer NOT NULL,
    taz_group_name character varying
);


ALTER TABLE <<schema>>.taz_groups OWNER TO postgres;

--
-- Name: taz_limit_groups; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.taz_limit_groups (
    taz_limit_group_id integer NOT NULL,
    taz_limit_group_name character varying(150)
);


ALTER TABLE <<schema>>.taz_limit_groups OWNER TO postgres;

--
-- Name: taz_limit_space_types; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.taz_limit_space_types (
    space_type_id integer NOT NULL,
    taz_limit_group_id integer NOT NULL
);


ALTER TABLE <<schema>>.taz_limit_space_types OWNER TO postgres;

--
-- Name: tazs; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.tazs (
    taz_number integer NOT NULL,
    luz_number integer NOT NULL,
    county_fips integer,
    geom public.geometry(MultiPolygon,3401)
);


ALTER TABLE <<schema>>.tazs OWNER TO postgres;

--
-- Name: tazs_by_taz_group; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.tazs_by_taz_group (
    taz_number integer NOT NULL,
    taz_group_id integer
);


ALTER TABLE <<schema>>.tazs_by_taz_group OWNER TO postgres;

--
-- Name: transition_constants_i; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.transition_constants_i (
    from_space_type_id integer NOT NULL,
    to_space_type_id integer NOT NULL,
    transition_constant double precision
);


ALTER TABLE <<schema>>.transition_constants_i OWNER TO postgres;

--
-- Name: transition_cost_codes; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.transition_cost_codes (
    cost_schedule_id integer NOT NULL,
    high_capacity_services_installation_cost double precision,
    low_capacity_services_installation_cost double precision,
    brownfield_cleanup_cost double precision,
    greenfield_preparation_cost double precision
);


ALTER TABLE <<schema>>.transition_cost_codes OWNER TO postgres;

--
-- Name: transition_costs; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.transition_costs (
    cost_schedule_id integer NOT NULL,
    space_type_id integer NOT NULL,
    demolition_cost double precision,
    renovation_cost double precision,
    addition_cost double precision,
    construction_cost double precision,
    renovation_derelict_cost double precision
);


ALTER TABLE <<schema>>.transition_costs OWNER TO postgres;

--
-- Name: zoning_permissions; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.zoning_permissions (
    zoning_rules_code integer NOT NULL,
    space_type_id integer NOT NULL,
    min_intensity_permitted double precision,
    max_intensity_permitted double precision,
    acknowledged_use boolean,
    penalty_acknowledged_space double precision,
    penalty_acknowledged_land double precision,
    services_requirement integer,
    maximum_development_size double precision,
    minimum_development_size double precision
);


ALTER TABLE <<schema>>.zoning_permissions OWNER TO postgres;

--
-- Name: zoning_rules_code_seq; Type: SEQUENCE; Schema: <<schema>>; Owner: postgres
--

CREATE SEQUENCE <<schema>>.zoning_rules_code_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE <<schema>>.zoning_rules_code_seq OWNER TO postgres;

--
-- Name: zoning_rules_i; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.zoning_rules_i (
    zoning_rules_code integer DEFAULT nextval('<<schema>>.zoning_rules_code_seq'::regclass) NOT NULL,
    zoning_rules_code_name character varying,
    no_change_possibilities boolean,
    dereliction_possibilities boolean,
    demolition_possibilities boolean,
    renovation_possibilities boolean,
    addition_possibilities boolean,
    new_development_possibilities boolean
);


ALTER TABLE <<schema>>.zoning_rules_i OWNER TO postgres;
