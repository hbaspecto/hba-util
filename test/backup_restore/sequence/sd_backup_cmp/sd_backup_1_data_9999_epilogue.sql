

--
-- Name: construction_commodities construction_commodities_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.construction_commodities
    ADD CONSTRAINT construction_commodities_pkey PRIMARY KEY (cc_commodity_id);


--
-- Name: density_step_points density_step_points_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.density_step_points
    ADD CONSTRAINT density_step_points_pkey PRIMARY KEY (space_type_id, step_point_number);


--
-- Name: development_fees development_fees_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.development_fees
    ADD CONSTRAINT development_fees_pkey PRIMARY KEY (fee_schedule_id, space_type_id);


--
-- Name: exchange_results exchange_results_index01; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.exchange_results
    ADD CONSTRAINT exchange_results_index01 PRIMARY KEY (commodity, luz);


--
-- Name: development_fee_schedules fee_sch_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.development_fee_schedules
    ADD CONSTRAINT fee_sch_pkey PRIMARY KEY (fee_schedule_id);


--
-- Name: local_effect_distances local_eff_dist_pk; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.local_effect_distances
    ADD CONSTRAINT local_eff_dist_pk PRIMARY KEY (pecas_parcel_num, local_effect_id, year_effective);


--
-- Name: local_effects local_effects_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.local_effects
    ADD CONSTRAINT local_effects_pkey PRIMARY KEY (local_effect_id);


--
-- Name: luzs luz_pk; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.luzs
    ADD CONSTRAINT luz_pk PRIMARY KEY (luz_number);


--
-- Name: parcel_cost_xref par_cost_pk; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.parcel_cost_xref
    ADD CONSTRAINT par_cost_pk PRIMARY KEY (pecas_parcel_num, year_effective);


--
-- Name: parcels parcel_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.parcels
    ADD CONSTRAINT parcel_pkey PRIMARY KEY (pecas_parcel_num);


--
-- Name: parcels_backup_with_geom parcels_backup_with_geom_table_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.parcels_backup_with_geom
    ADD CONSTRAINT parcels_backup_with_geom_table_pkey PRIMARY KEY (pecas_parcel_num);


--
-- Name: parcel_fee_xref parcelxref_pk; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.parcel_fee_xref
    ADD CONSTRAINT parcelxref_pk PRIMARY KEY (pecas_parcel_num, year_effective);


--
-- Name: phasing_plan_current phasing_plan_current_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.phasing_plan_current
    ADD CONSTRAINT phasing_plan_current_pkey PRIMARY KEY (plan_id);


--
-- Name: phasing_plan_xref phasing_plan_xref_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.phasing_plan_xref
    ADD CONSTRAINT phasing_plan_xref_pkey PRIMARY KEY (pecas_parcel_num);


--
-- Name: phasing_plans phasing_plans_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.phasing_plans
    ADD CONSTRAINT phasing_plans_pkey PRIMARY KEY (plan_id);


--
-- Name: current_year_table pk_cyr; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.current_year_table
    ADD CONSTRAINT pk_cyr PRIMARY KEY (current_year);


--
-- Name: local_effect_parameters pk_local_effect_param; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.local_effect_parameters
    ADD CONSTRAINT pk_local_effect_param PRIMARY KEY (local_effect_id, space_type_id);


--
-- Name: parcels_backup pk_parcel_backup; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.parcels_backup
    ADD CONSTRAINT pk_parcel_backup PRIMARY KEY (pecas_parcel_num);


--
-- Name: sitespec_parcels pk_sitespec_parcels; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.sitespec_parcels
    ADD CONSTRAINT pk_sitespec_parcels PRIMARY KEY (pecas_parcel_num, siteid, space_type_id);


--
-- Name: parcel_zoning_xref pz_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.parcel_zoning_xref
    ADD CONSTRAINT pz_pkey PRIMARY KEY (pecas_parcel_num, year_effective);


--
-- Name: random_seeds random_seeds_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.random_seeds
    ADD CONSTRAINT random_seeds_pkey PRIMARY KEY (pecas_parcel_num, year_effective);


--
-- Name: sitespec_site_amounts site_spec_totals_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.sitespec_site_amounts
    ADD CONSTRAINT site_spec_totals_pkey PRIMARY KEY (site_id, space_type_id, year);


--
-- Name: sitespec_totals sitespec_totals_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.sitespec_totals
    ADD CONSTRAINT sitespec_totals_pkey PRIMARY KEY (space_type_id, year_effective);


--
-- Name: space_taz_limits space_taz_limits_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.space_taz_limits
    ADD CONSTRAINT space_taz_limits_pkey PRIMARY KEY (taz_group_id, taz_limit_group_id, year_effective);


--
-- Name: space_types_group space_types_group_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.space_types_group
    ADD CONSTRAINT space_types_group_pkey PRIMARY KEY (space_types_group_id);


--
-- Name: space_types_i spacetypeid_PK; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.space_types_i
    ADD CONSTRAINT "spacetypeid_PK" PRIMARY KEY (space_type_id);


--
-- Name: taz_group_constants taz_group_constants_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.taz_group_constants
    ADD CONSTRAINT taz_group_constants_pkey PRIMARY KEY (taz_group_id);


--
-- Name: taz_group_space_constants taz_group_space_constants_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.taz_group_space_constants
    ADD CONSTRAINT taz_group_space_constants_pkey PRIMARY KEY (taz_group_id, space_type_id);


--
-- Name: taz_groups taz_groups_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.taz_groups
    ADD CONSTRAINT taz_groups_pkey PRIMARY KEY (taz_group_id);


--
-- Name: taz_limit_groups taz_limit_groups_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.taz_limit_groups
    ADD CONSTRAINT taz_limit_groups_pkey PRIMARY KEY (taz_limit_group_id);


--
-- Name: taz_limit_space_types taz_limit_space_types_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.taz_limit_space_types
    ADD CONSTRAINT taz_limit_space_types_pkey PRIMARY KEY (space_type_id);


--
-- Name: tazs taz_pk; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.tazs
    ADD CONSTRAINT taz_pk PRIMARY KEY (taz_number);


--
-- Name: tazs_by_taz_group tazs_by_taz_group_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.tazs_by_taz_group
    ADD CONSTRAINT tazs_by_taz_group_pkey PRIMARY KEY (taz_number);


--
-- Name: transition_constants_i trans_cons_pk; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.transition_constants_i
    ADD CONSTRAINT trans_cons_pk PRIMARY KEY (from_space_type_id, to_space_type_id);


--
-- Name: transition_cost_codes transition_costcodes_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.transition_cost_codes
    ADD CONSTRAINT transition_costcodes_pkey PRIMARY KEY (cost_schedule_id);


--
-- Name: transition_costs transition_costs_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.transition_costs
    ADD CONSTRAINT transition_costs_pkey PRIMARY KEY (cost_schedule_id, space_type_id);


--
-- Name: zoning_permissions zon_perm_pki; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.zoning_permissions
    ADD CONSTRAINT zon_perm_pki PRIMARY KEY (zoning_rules_code, space_type_id);


--
-- Name: zoning_rules_i zoning_rulesi_pkey; Type: CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.zoning_rules_i
    ADD CONSTRAINT zoning_rulesi_pkey PRIMARY KEY (zoning_rules_code);


--
-- Name: luzs_geom_idx; Type: INDEX; Schema: <<schema>>; Owner: postgres
--

CREATE INDEX luzs_geom_idx ON <<schema>>.luzs USING gist (geom);


--
-- Name: parcel_cost_pid_idx; Type: INDEX; Schema: <<schema>>; Owner: postgres
--

CREATE INDEX parcel_cost_pid_idx ON <<schema>>.parcel_cost_xref USING hash (pecas_parcel_num);


--
-- Name: parcel_cost_xref_ix_cost_on_year; Type: INDEX; Schema: <<schema>>; Owner: postgres
--

CREATE INDEX parcel_cost_xref_ix_cost_on_year ON <<schema>>.parcel_cost_xref USING btree (year_effective);


--
-- Name: parcel_fee_pid_idx; Type: INDEX; Schema: <<schema>>; Owner: postgres
--

CREATE INDEX parcel_fee_pid_idx ON <<schema>>.parcel_fee_xref USING hash (pecas_parcel_num);


--
-- Name: parcel_fee_xref_ix_fee_on_year; Type: INDEX; Schema: <<schema>>; Owner: postgres
--

CREATE INDEX parcel_fee_xref_ix_fee_on_year ON <<schema>>.parcel_fee_xref USING btree (year_effective);


--
-- Name: parcel_zoning_pid_idx; Type: INDEX; Schema: <<schema>>; Owner: postgres
--

CREATE INDEX parcel_zoning_pid_idx ON <<schema>>.parcel_zoning_xref USING hash (pecas_parcel_num);


--
-- Name: parcel_zoning_xref_ix_year; Type: INDEX; Schema: <<schema>>; Owner: postgres
--

CREATE INDEX parcel_zoning_xref_ix_year ON <<schema>>.parcel_zoning_xref USING btree (year_effective);


--
-- Name: parcels_backup_pecas_num_idx_hash; Type: INDEX; Schema: <<schema>>; Owner: postgres
--

CREATE INDEX parcels_backup_pecas_num_idx_hash ON <<schema>>.parcels_backup USING hash (pecas_parcel_num);


--
-- Name: parcels_backup_with_geom_geom_idx; Type: INDEX; Schema: <<schema>>; Owner: postgres
--

CREATE INDEX parcels_backup_with_geom_geom_idx ON <<schema>>.parcels_backup_with_geom USING gist (geom);


--
-- Name: parcels_backup_with_geom_pecas_num_idx_hash; Type: INDEX; Schema: <<schema>>; Owner: postgres
--

CREATE INDEX parcels_backup_with_geom_pecas_num_idx_hash ON <<schema>>.parcels_backup_with_geom USING hash (pecas_parcel_num);


--
-- Name: parcels_pecas_num_idx_hash; Type: INDEX; Schema: <<schema>>; Owner: postgres
--

CREATE INDEX parcels_pecas_num_idx_hash ON <<schema>>.parcels USING hash (pecas_parcel_num);


--
-- Name: tazs_geom_idx; Type: INDEX; Schema: <<schema>>; Owner: postgres
--

CREATE INDEX tazs_geom_idx ON <<schema>>.tazs USING gist (geom);


--
-- Name: parcel_cost_xref cost_id_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.parcel_cost_xref
    ADD CONSTRAINT cost_id_fkey FOREIGN KEY (cost_schedule_id) REFERENCES <<schema>>.transition_cost_codes(cost_schedule_id);


--
-- Name: density_step_points density_step_points_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.density_step_points
    ADD CONSTRAINT density_step_points_fkey FOREIGN KEY (space_type_id) REFERENCES <<schema>>.space_types_i(space_type_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: development_fees dev_sched_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.development_fees
    ADD CONSTRAINT dev_sched_fkey FOREIGN KEY (fee_schedule_id) REFERENCES <<schema>>.development_fee_schedules(fee_schedule_id);


--
-- Name: parcel_fee_xref dev_sched_id_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.parcel_fee_xref
    ADD CONSTRAINT dev_sched_id_fkey FOREIGN KEY (fee_schedule_id) REFERENCES <<schema>>.development_fee_schedules(fee_schedule_id);


--
-- Name: development_fees feespace_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.development_fees
    ADD CONSTRAINT feespace_fkey FOREIGN KEY (space_type_id) REFERENCES <<schema>>.space_types_i(space_type_id);


--
-- Name: transition_costs foreign_key01; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.transition_costs
    ADD CONSTRAINT foreign_key01 FOREIGN KEY (space_type_id) REFERENCES <<schema>>.space_types_i(space_type_id);


--
-- Name: transition_constants_i from_sptype_id_fk; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.transition_constants_i
    ADD CONSTRAINT from_sptype_id_fk FOREIGN KEY (from_space_type_id) REFERENCES <<schema>>.space_types_i(space_type_id);


--
-- Name: local_effect_parameters local_eff_id_fk; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.local_effect_parameters
    ADD CONSTRAINT local_eff_id_fk FOREIGN KEY (local_effect_id) REFERENCES <<schema>>.local_effects(local_effect_id);


--
-- Name: local_effect_distances local_effect_id_fk; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.local_effect_distances
    ADD CONSTRAINT local_effect_id_fk FOREIGN KEY (local_effect_id) REFERENCES <<schema>>.local_effects(local_effect_id);


--
-- Name: tazs luz_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.tazs
    ADD CONSTRAINT luz_fkey FOREIGN KEY (luz_number) REFERENCES <<schema>>.luzs(luz_number);


--
-- Name: parcel_fee_xref parcel_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.parcel_fee_xref
    ADD CONSTRAINT parcel_fkey FOREIGN KEY (pecas_parcel_num) REFERENCES <<schema>>.parcels(pecas_parcel_num) ON DELETE CASCADE;


--
-- Name: parcels parcel_taz_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.parcels
    ADD CONSTRAINT parcel_taz_fkey FOREIGN KEY (taz) REFERENCES <<schema>>.tazs(taz_number);


--
-- Name: parcel_cost_xref parcost_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.parcel_cost_xref
    ADD CONSTRAINT parcost_fkey FOREIGN KEY (pecas_parcel_num) REFERENCES <<schema>>.parcels(pecas_parcel_num) ON DELETE CASCADE;


--
-- Name: local_effect_distances pecas_parcel_num_fk; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.local_effect_distances
    ADD CONSTRAINT pecas_parcel_num_fk FOREIGN KEY (pecas_parcel_num) REFERENCES <<schema>>.parcels(pecas_parcel_num) ON DELETE CASCADE;


--
-- Name: phasing_plan_current phasing_plan_current_plan_id_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.phasing_plan_current
    ADD CONSTRAINT phasing_plan_current_plan_id_fkey FOREIGN KEY (plan_id) REFERENCES <<schema>>.phasing_plans(plan_id);


--
-- Name: phasing_plan_xref phasing_plan_xref_pecas_parcel_num_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.phasing_plan_xref
    ADD CONSTRAINT phasing_plan_xref_pecas_parcel_num_fkey FOREIGN KEY (pecas_parcel_num) REFERENCES <<schema>>.parcels(pecas_parcel_num) ON DELETE CASCADE;


--
-- Name: phasing_plan_xref phasing_plan_xref_plan_id_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.phasing_plan_xref
    ADD CONSTRAINT phasing_plan_xref_plan_id_fkey FOREIGN KEY (plan_id) REFERENCES <<schema>>.phasing_plans(plan_id);


--
-- Name: phasing_plan_xref phasing_plan_xref_zoning_rules_code_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.phasing_plan_xref
    ADD CONSTRAINT phasing_plan_xref_zoning_rules_code_fkey FOREIGN KEY (zoning_rules_code) REFERENCES <<schema>>.zoning_rules_i(zoning_rules_code);


--
-- Name: parcel_zoning_xref pp_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.parcel_zoning_xref
    ADD CONSTRAINT pp_fkey FOREIGN KEY (pecas_parcel_num) REFERENCES <<schema>>.parcels(pecas_parcel_num) ON DELETE CASCADE;


--
-- Name: sitespec_site_amounts project_space_type_id_fk; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.sitespec_site_amounts
    ADD CONSTRAINT project_space_type_id_fk FOREIGN KEY (space_type_id) REFERENCES <<schema>>.space_types_i(space_type_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: random_seeds random_seeds_pecas_parcel_num_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.random_seeds
    ADD CONSTRAINT random_seeds_pecas_parcel_num_fkey FOREIGN KEY (pecas_parcel_num) REFERENCES <<schema>>.parcels(pecas_parcel_num);


--
-- Name: sitespec_parcels sitespec_parcel_num_fk; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.sitespec_parcels
    ADD CONSTRAINT sitespec_parcel_num_fk FOREIGN KEY (pecas_parcel_num) REFERENCES <<schema>>.parcels(pecas_parcel_num);


--
-- Name: construction_commodities sp_group_fk; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.construction_commodities
    ADD CONSTRAINT sp_group_fk FOREIGN KEY (space_types_group_id) REFERENCES <<schema>>.space_types_group(space_types_group_id);


--
-- Name: space_taz_limits space_taz_limits_group_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.space_taz_limits
    ADD CONSTRAINT space_taz_limits_group_fkey FOREIGN KEY (taz_limit_group_id) REFERENCES <<schema>>.taz_limit_groups(taz_limit_group_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: space_taz_limits space_taz_limits_taz_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.space_taz_limits
    ADD CONSTRAINT space_taz_limits_taz_fkey FOREIGN KEY (taz_group_id) REFERENCES <<schema>>.taz_groups(taz_group_id);


--
-- Name: space_types_i space_type_grp_id_fk; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.space_types_i
    ADD CONSTRAINT space_type_grp_id_fk FOREIGN KEY (space_type_group_id) REFERENCES <<schema>>.space_types_group(space_types_group_id);


--
-- Name: parcels space_typeid_fk; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.parcels
    ADD CONSTRAINT space_typeid_fk FOREIGN KEY (space_type_id) REFERENCES <<schema>>.space_types_i(space_type_id);


--
-- Name: space_to_commodity spacetype_com_fk; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.space_to_commodity
    ADD CONSTRAINT spacetype_com_fk FOREIGN KEY (space_type_id) REFERENCES <<schema>>.space_types_i(space_type_id) ON UPDATE CASCADE;


--
-- Name: local_effect_parameters spce_type_effect_fk; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.local_effect_parameters
    ADD CONSTRAINT spce_type_effect_fk FOREIGN KEY (space_type_id) REFERENCES <<schema>>.space_types_i(space_type_id);


--
-- Name: tazs_by_taz_group taz_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.tazs_by_taz_group
    ADD CONSTRAINT taz_fkey FOREIGN KEY (taz_number) REFERENCES <<schema>>.tazs(taz_number) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: taz_group_constants taz_group_constants_taz_group_id_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.taz_group_constants
    ADD CONSTRAINT taz_group_constants_taz_group_id_fkey FOREIGN KEY (taz_group_id) REFERENCES <<schema>>.taz_groups(taz_group_id);


--
-- Name: tazs_by_taz_group taz_group_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.tazs_by_taz_group
    ADD CONSTRAINT taz_group_fkey FOREIGN KEY (taz_group_id) REFERENCES <<schema>>.taz_groups(taz_group_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: taz_group_space_constants taz_group_space_constants_space_type_id_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.taz_group_space_constants
    ADD CONSTRAINT taz_group_space_constants_space_type_id_fkey FOREIGN KEY (space_type_id) REFERENCES <<schema>>.space_types_i(space_type_id);


--
-- Name: taz_group_space_constants taz_group_space_constants_taz_group_id_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.taz_group_space_constants
    ADD CONSTRAINT taz_group_space_constants_taz_group_id_fkey FOREIGN KEY (taz_group_id) REFERENCES <<schema>>.taz_groups(taz_group_id);


--
-- Name: taz_limit_space_types taz_limit_space_type_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.taz_limit_space_types
    ADD CONSTRAINT taz_limit_space_type_fkey FOREIGN KEY (space_type_id) REFERENCES <<schema>>.space_types_i(space_type_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: taz_limit_space_types taz_limit_space_types_group_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.taz_limit_space_types
    ADD CONSTRAINT taz_limit_space_types_group_fkey FOREIGN KEY (taz_limit_group_id) REFERENCES <<schema>>.taz_limit_groups(taz_limit_group_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transition_constants_i to_sptype_id_fk; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.transition_constants_i
    ADD CONSTRAINT to_sptype_id_fk FOREIGN KEY (to_space_type_id) REFERENCES <<schema>>.space_types_i(space_type_id);


--
-- Name: transition_costs transition_cost_code_fk; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.transition_costs
    ADD CONSTRAINT transition_cost_code_fk FOREIGN KEY (cost_schedule_id) REFERENCES <<schema>>.transition_cost_codes(cost_schedule_id);


--
-- Name: zoning_permissions zoingspace_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.zoning_permissions
    ADD CONSTRAINT zoingspace_fkey FOREIGN KEY (space_type_id) REFERENCES <<schema>>.space_types_i(space_type_id);


--
-- Name: zoning_permissions zon_rul_fki; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.zoning_permissions
    ADD CONSTRAINT zon_rul_fki FOREIGN KEY (zoning_rules_code) REFERENCES <<schema>>.zoning_rules_i(zoning_rules_code);


--
-- Name: parcel_zoning_xref zrc_fkey; Type: FK CONSTRAINT; Schema: <<schema>>; Owner: postgres
--

ALTER TABLE ONLY <<schema>>.parcel_zoning_xref
    ADD CONSTRAINT zrc_fkey FOREIGN KEY (zoning_rules_code) REFERENCES <<schema>>.zoning_rules_i(zoning_rules_code);


--
-- PostgreSQL database dump complete
