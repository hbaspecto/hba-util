--

--
-- PostgreSQL database dump
--

-- Dumped from database version 10.9 (Ubuntu 10.9-1.pgdg18.04+1)
-- Dumped by pg_dump version 10.15 (Ubuntu 10.15-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET search_path = <<schema>>, pg_catalog;
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- Name: development_events; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.development_events (
    event_type character(2) NOT NULL,
    parcel_id character varying,
    original_pecas_parcel_num bigint NOT NULL,
    new_pecas_parcel_num bigint NOT NULL,
    available_services integer NOT NULL,
    old_space_type_id integer NOT NULL,
    new_space_type_id integer NOT NULL,
    old_space_quantity double precision,
    new_space_quantity double precision NOT NULL,
    old_year_built integer,
    new_year_built integer NOT NULL,
    land_area double precision NOT NULL,
    old_is_derelict boolean,
    new_is_derelict boolean,
    old_is_brownfield boolean,
    new_is_brownfield boolean,
    zoning_rules_code integer NOT NULL,
    taz integer NOT NULL
);


ALTER TABLE <<schema>>.development_events OWNER TO postgres;

--
-- Name: development_events_history; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.development_events_history (
    year_run integer,
    event_type character(2),
    parcel_id character varying,
    original_pecas_parcel_num bigint,
    new_pecas_parcel_num bigint,
    available_services integer,
    old_space_type_id integer,
    new_space_type_id integer,
    old_space_quantity double precision,
    new_space_quantity double precision,
    old_year_built integer,
    new_year_built integer,
    land_area double precision,
    old_is_derelict boolean,
    new_is_derelict boolean,
    old_is_brownfield boolean,
    new_is_brownfield boolean,
    zoning_rules_code integer,
    taz integer
);


ALTER TABLE <<schema>>.development_events_history OWNER TO postgres;

--
-- Name: development_history_summary; Type: VIEW; Schema: <<schema>>; Owner: postgres
--

CREATE VIEW <<schema>>.development_history_summary AS
 SELECT development_events_history.year_run,
    development_events_history.taz,
    development_events_history.zoning_rules_code,
    development_events_history.event_type,
    count(*) AS number_of_events,
    development_events_history.old_space_type_id,
    development_events_history.new_space_type_id,
    sum(development_events_history.old_space_quantity) AS old_space_total,
    sum(development_events_history.new_space_quantity) AS new_space_total,
    avg(development_events_history.old_year_built) AS avg_old_year,
    avg(development_events_history.new_year_built) AS avg_new_year,
    sum(development_events_history.land_area) AS sum_land
   FROM <<schema>>.development_events_history
  GROUP BY development_events_history.year_run, development_events_history.event_type, development_events_history.old_space_type_id, development_events_history.new_space_type_id, development_events_history.zoning_rules_code, development_events_history.taz
  ORDER BY development_events_history.year_run, development_events_history.taz, development_events_history.zoning_rules_code, development_events_history.event_type;


ALTER TABLE <<schema>>.development_history_summary OWNER TO postgres;

--
-- Name: floorspacei_view; Type: VIEW; Schema: <<schema>>; Owner: postgres
--

CREATE VIEW <<schema>>.floorspacei_view AS
 SELECT p.taz AS "TAZ",
    c.aa_commodity AS "Commodity",
    sum((p.space_quantity * c.weight)) AS "Quantity"
   FROM <<schema>>.parcels p,
    <<schema>>.space_types_i s,
    <<schema>>.space_to_commodity c
  WHERE ((p.space_type_id = s.space_type_id) AND (s.space_type_id = c.space_type_id))
  GROUP BY p.taz, c.aa_commodity;


ALTER TABLE <<schema>>.floorspacei_view OWNER TO postgres;

--
-- Name: local_effect_distances_most_recent; Type: MATERIALIZED VIEW; Schema: <<schema>>; Owner: postgres
--

CREATE MATERIALIZED VIEW <<schema>>.local_effect_distances_most_recent AS
 SELECT DISTINCT ON (local_effect_distances.pecas_parcel_num, local_effect_distances.local_effect_id) local_effect_distances.pecas_parcel_num,
    local_effect_distances.local_effect_id,
    local_effect_distances.local_effect_distance,
    local_effect_distances.year_effective
   FROM <<schema>>.local_effect_distances,
    <<schema>>.current_year_table
  WHERE (local_effect_distances.year_effective <= current_year_table.current_year)
  ORDER BY local_effect_distances.pecas_parcel_num, local_effect_distances.local_effect_id, local_effect_distances.year_effective DESC
  WITH NO DATA;


ALTER TABLE <<schema>>.local_effect_distances_most_recent OWNER TO postgres;

--
-- Name: most_recent_cost_year; Type: VIEW; Schema: <<schema>>; Owner: postgres
--

CREATE VIEW <<schema>>.most_recent_cost_year AS
 SELECT parcel_cost_xref.pecas_parcel_num,
    max(parcel_cost_xref.year_effective) AS current_cost_year
   FROM <<schema>>.parcel_cost_xref,
    <<schema>>.current_year_table
  WHERE (parcel_cost_xref.year_effective <= current_year_table.current_year)
  GROUP BY parcel_cost_xref.pecas_parcel_num;


ALTER TABLE <<schema>>.most_recent_cost_year OWNER TO postgres;

--
-- Name: most_recent_fee_year; Type: VIEW; Schema: <<schema>>; Owner: postgres
--

CREATE VIEW <<schema>>.most_recent_fee_year AS
 SELECT parcel_fee_xref.pecas_parcel_num,
    max(parcel_fee_xref.year_effective) AS current_fee_year
   FROM <<schema>>.parcel_fee_xref,
    <<schema>>.current_year_table
  WHERE (parcel_fee_xref.year_effective <= current_year_table.current_year)
  GROUP BY parcel_fee_xref.pecas_parcel_num;


ALTER TABLE <<schema>>.most_recent_fee_year OWNER TO postgres;

--
-- Name: most_recent_local_effect_with_taz; Type: VIEW; Schema: <<schema>>; Owner: postgres
--

CREATE VIEW <<schema>>.most_recent_local_effect_with_taz AS
 SELECT le.pecas_parcel_num,
    p.taz,
    le.local_effect_id,
    le.local_effect_distance,
    max(le.year_effective) AS current_local_effect_year
   FROM <<schema>>.local_effect_distances le,
    <<schema>>.current_year_table cy,
    <<schema>>.parcels p
  WHERE ((p.pecas_parcel_num = le.pecas_parcel_num) AND (le.year_effective <= cy.current_year))
  GROUP BY le.pecas_parcel_num, p.taz, le.local_effect_id, le.local_effect_distance;


ALTER TABLE <<schema>>.most_recent_local_effect_with_taz OWNER TO postgres;

--
-- Name: most_recent_local_effect_year; Type: VIEW; Schema: <<schema>>; Owner: postgres
--

CREATE VIEW <<schema>>.most_recent_local_effect_year AS
 SELECT le.pecas_parcel_num,
    max(le.year_effective) AS current_local_effect_year
   FROM <<schema>>.local_effect_distances le,
    <<schema>>.current_year_table cy
  WHERE (le.year_effective <= cy.current_year)
  GROUP BY le.pecas_parcel_num;


ALTER TABLE <<schema>>.most_recent_local_effect_year OWNER TO postgres;

--
-- Name: most_recent_taz_limit_year; Type: VIEW; Schema: <<schema>>; Owner: postgres
--

CREATE VIEW <<schema>>.most_recent_taz_limit_year AS
 SELECT space_taz_limits.taz_group_id,
    max(space_taz_limits.year_effective) AS current_limit_year
   FROM (<<schema>>.space_taz_limits
     JOIN <<schema>>.current_year_table ON ((space_taz_limits.year_effective <= current_year_table.current_year)))
  GROUP BY space_taz_limits.taz_group_id;


ALTER TABLE <<schema>>.most_recent_taz_limit_year OWNER TO postgres;

--
-- Name: most_recent_zoning_year; Type: VIEW; Schema: <<schema>>; Owner: postgres
--

CREATE VIEW <<schema>>.most_recent_zoning_year AS
 SELECT parcel_zoning_xref.pecas_parcel_num,
    max(parcel_zoning_xref.year_effective) AS current_zoning_year
   FROM <<schema>>.parcel_zoning_xref,
    <<schema>>.current_year_table
  WHERE (parcel_zoning_xref.year_effective <= current_year_table.current_year)
  GROUP BY parcel_zoning_xref.pecas_parcel_num;


ALTER TABLE <<schema>>.most_recent_zoning_year OWNER TO postgres;

--
-- Name: parcels_random_number; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.parcels_random_number (
    pecas_parcel_num bigint,
    rand double precision
);


ALTER TABLE <<schema>>.parcels_random_number OWNER TO postgres;

--
-- Name: parcels_temp; Type: TABLE; Schema: <<schema>>; Owner: postgres
--

CREATE TABLE <<schema>>.parcels_temp (
    parcel_id character varying,
    pecas_parcel_num bigint NOT NULL,
    year_built integer,
    taz integer,
    space_type_id integer,
    space_quantity double precision,
    land_area double precision,
    available_services_code integer,
    zoning_rules_code integer,
    cost_schedule_id integer,
    fee_schedule_id integer,
    is_derelict boolean,
    is_brownfield boolean,
    randnum double precision
);


ALTER TABLE <<schema>>.parcels_temp OWNER TO postgres;

--
-- Name: parcels_view; Type: VIEW; Schema: <<schema>>; Owner: postgres
--

CREATE VIEW <<schema>>.parcels_view AS
 SELECT fp.parcel_id,
    fp.pecas_parcel_num,
    fp.year_built,
    fp.taz,
    fp.space_type_id,
    fp.space_quantity,
    fp.land_area,
    fp.available_services_code,
    fp.is_derelict,
    fp.is_brownfield,
    zxref.zoning_rules_code,
    costxref.cost_schedule_id,
    feexref.fee_schedule_id
   FROM <<schema>>.parcels fp,
    <<schema>>.most_recent_zoning_year z,
    <<schema>>.most_recent_fee_year f,
    <<schema>>.most_recent_cost_year c,
    <<schema>>.parcel_zoning_xref zxref,
    <<schema>>.parcel_fee_xref feexref,
    <<schema>>.parcel_cost_xref costxref
  WHERE ((fp.pecas_parcel_num = z.pecas_parcel_num) AND (fp.pecas_parcel_num = zxref.pecas_parcel_num) AND (zxref.year_effective = z.current_zoning_year) AND (fp.pecas_parcel_num = feexref.pecas_parcel_num) AND (fp.pecas_parcel_num = f.pecas_parcel_num) AND (feexref.year_effective = f.current_fee_year) AND (fp.pecas_parcel_num = costxref.pecas_parcel_num) AND (fp.pecas_parcel_num = c.pecas_parcel_num) AND (costxref.year_effective = c.current_cost_year));


ALTER TABLE <<schema>>.parcels_view OWNER TO postgres;

--
-- Name: transition_constants_i_view; Type: VIEW; Schema: <<schema>>; Owner: postgres
--

CREATE VIEW <<schema>>.transition_constants_i_view AS
 SELECT a.space_type_name AS from_space_type_name,
    b.space_type_name AS to_space_type_name,
    tr.transition_constant
   FROM <<schema>>.space_types_i a,
    <<schema>>.space_types_i b,
    <<schema>>.transition_constants_i tr
  WHERE ((a.space_type_id = tr.from_space_type_id) AND (b.space_type_id = tr.to_space_type_id));


ALTER TABLE <<schema>>.transition_constants_i_view OWNER TO postgres;
