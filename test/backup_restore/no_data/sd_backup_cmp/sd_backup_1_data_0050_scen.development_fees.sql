

--
-- Data for Name: development_fees; Type: TABLE DATA; Schema: <<schema>>; Owner: postgres
--

COPY <<schema>>.development_fees (fee_schedule_id, space_type_id, development_fee_per_unit_space_initial, development_fee_per_unit_land_initial, development_fee_per_unit_space_ongoing, development_fee_per_unit_land_ongoing) FROM stdin;
\.
