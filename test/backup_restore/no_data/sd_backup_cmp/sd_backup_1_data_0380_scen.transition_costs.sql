

--
-- Data for Name: transition_costs; Type: TABLE DATA; Schema: <<schema>>; Owner: postgres
--

COPY <<schema>>.transition_costs (cost_schedule_id, space_type_id, demolition_cost, renovation_cost, addition_cost, construction_cost, renovation_derelict_cost) FROM stdin;
\.
