

--
-- Data for Name: parcels_backup_with_geom; Type: TABLE DATA; Schema: <<schema>>; Owner: postgres
--

COPY <<schema>>.parcels_backup_with_geom (parcel_id, pecas_parcel_num, year_built, taz, space_type_id, space_quantity, land_area, available_services_code, is_derelict, is_brownfield, geom) FROM stdin;
\.
