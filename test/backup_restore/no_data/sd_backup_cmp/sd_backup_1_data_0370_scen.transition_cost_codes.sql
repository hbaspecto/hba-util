

--
-- Data for Name: transition_cost_codes; Type: TABLE DATA; Schema: <<schema>>; Owner: postgres
--

COPY <<schema>>.transition_cost_codes (cost_schedule_id, high_capacity_services_installation_cost, low_capacity_services_installation_cost, brownfield_cleanup_cost, greenfield_preparation_cost) FROM stdin;
\.
